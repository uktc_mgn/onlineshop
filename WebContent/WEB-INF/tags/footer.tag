<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@tag description="User Page template" pageEncoding="UTF-8" %>

<link rel="stylesheet" href="/indexCss/footerTagStyle.css">
<link rel="stylesheet" href="/indexCss/media.css">
<div class="footer">
    <div class="media">
        <p>Social Media:</p>
        <div class="links col-xs-12 col-sm-4 col-md-2 col-lg-2">
            <a href="https://www.facebook.com" id="facebook"><i class="fab fa-facebook"></i></a>
            <a href="https://www.twitter.com" id="twitter"><i class="fab fa-twitter"></i></a>
            <a href="https://www.instagram.com" id="instagram"><i class="fab fa-instagram"></i></a>
        </div>
    </div>

    <div class="c">
        <p>Contacts:</p>
        <div class="contacts col-xs-12 col-sm-4 col-md-2 col-lg-2">
            <a href="tel:012345678" id="phone"><i class="fas fa-phone"></i></a>
            <a href="mailto:somemail@gmail.com" id="mail"><i class="fas fa-envelope"></i></a>
            <a href="https://maps.google.com" id="location"><i class="fas fa-map-marker"></i></a>
        </div>
    </div>
    <p class="siteInfo">Sitename.com &copy; 2018</p>
</div>