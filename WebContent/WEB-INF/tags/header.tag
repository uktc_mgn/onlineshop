<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@tag description="User Page template" pageEncoding="UTF-8" %>
<%@tag import="Model.User" %>
<%@attribute name="user" required="false" type="Model.User" %>
<%@attribute name="logged" required="true" %>
<%@attribute name="title" required="false" %>

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.10/css/all.css"
          integrity="sha384-+d0P83n9kaQMCwj8F4RJB66tzIwOKmrdb46+porD/OvrJ+37WqIM7UoBtwHO6Nlg"
          crossorigin="anonymous">
    <link rel="stylesheet" href="/indexCss/headerTagStyle.css">
    <link rel="stylesheet" href="/indexCss/media.css">
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
    <title>Shop - <c:out value="${title}"/></title>
    <c:if test="${not empty sessionScope.Error}">
        <script>
            window.addEventListener('load', function () {
                if (!(document.querySelector(".notification-box").classList.contains("show") ||
                    document.querySelector(".notification-box").classList.contains("flip") ||
                    document.querySelector(".notification-box").classList.contains("hide"))) {

                    document.querySelector(".notification-box").classList.add("show");
                    setTimeout(function () {
                        document.querySelector(".notification-box").classList.remove("show");
                        document.querySelector(".notification-box").classList.add("flip");
                        setTimeout(function () {
                            document.querySelector(".notification-text").classList.add("show");
                        }, 300);
                        setTimeout(function () {
                            document.querySelector(".notification-box").classList.remove("flip");
                            document.querySelector(".notification-box").classList.add("unflip");
                            setTimeout(function () {
                                document.querySelector(".notification-text").classList.remove("show");
                            }, 200);
                            setTimeout(function () {
                                document.querySelector(".notification-box").classList.remove("unflip");
                                document.querySelector(".notification-box").classList.add("hide");
                                setTimeout(function () {
                                    document.querySelector(".notification-box").classList.remove("hide");
                                }, 750);
                            }, 750);
                        }, 3000);
                    }, 750);
                }
            });

            function openMenu() {
                document.getElementById("sidebar").classList.add("sidebar-animate-left");
                document.getElementById("sidebar").classList.remove("sidebar-animate-right");
                setTimeout(function () {
                    document.getElementById("sidebar").style.display = "block";
                    document.getElementById("overlay").style.display = "block";
                }, 5);
            }

            function closeMenu() {
                document.getElementById("sidebar").classList.remove("sidebar-animate-left");
                setTimeout(function () {
                    document.getElementById("sidebar").classList.add("sidebar-animate-right");
                }, 2);

                setTimeout(function () {
                    document.getElementById("sidebar").style.display = "none";
                    document.getElementById("overlay").style.display = "none";
                }, 410);
            }
        </script>
    </c:if>
</head>
<div class="main">
    <div class="overlay animate-opacity" onclick="closeMenu()" id="overlay"></div>
    <nav class="menu">
        <ul class="header">
            <i class="fas fa-bars" onclick="openMenu()"></i>
            <li><a href="/"><p> Home</p></a></li>
            <li><a href="/products"><p> Products</p></a></li>
            <c:choose>
                <c:when test="${logged}">
                    <li style="float: right"><a href="/logout"><p>Logout</p></a></li>
                    <li style="float: right"><a href="/profile"><p>Profile</p></a></li>
                    <c:if test="${user.getRole()>=1}">
                        <li style="float: right"><a href="/products/create"><p>Create
                            Product</p></a></li>
                    </c:if>
                    <c:if test="${user.getRole()>=2}">
                        <li style="float: right;"><a href="/products/status"><p>Update Status</p></a></li>
                        <li style="float: right"><a href="/users"><p>Edit Users</p>
                        </a></li>
                    </c:if>
                    <li style="float: left"><a href="/cart"><p>My Cart</p></a></li>
                </c:when>
                <c:otherwise>
                    <li style="float: right;"><a href="/login"><p>Login</p></a></li>
                    <li style="float: right;"><a href="/register"><p>Register</p></a></li>

                </c:otherwise>

            </c:choose>
        </ul>
    </nav>
    <div class="sidebar sidebar-animate-left bar-block" id="sidebar" style="display: none; z-index: 5">
        <div class="menuS col-xs-12 col-sm-12 col-lg-12 col-lg-12">
            <button class="bar-item button large close-button" onclick="closeMenu()"><i class="fas fa-times"></i>
            </button>
            <a class="bar-item button" href="/"><i class="fas fa-home"></i> Home</a>
            <a class="bar-item button" href="/products"><i class="fas fa-box-open"></i> Products</a>
            <c:choose>
                <c:when test="${logged}">
                    <c:if test="${user.getRole()>=1}">
                        <a class="bar-item button" href="/products/create"><i class="fas fa-plus-square"></i> Create
                            Products</a>
                    </c:if>
                    <c:if test="${user.getRole()>=2}">
                        <a class="bar-item button" href="/products/status"><i class="fas fa-edit"></i> Update Status</a>
                        <a class="bar-item button" href="/users"><i class="fas fa-users"></i> Edit Users</a>
                    </c:if>
                    <a class="bar-item button" href="/cart"><i class="fas fa-shopping-cart"></i> My Cart</a>
                    <a class="bar-item button" href="/profile"><i class="fas fa-user"></i> Profile</a>
                    <a class="bar-item button" href="/logout"><i class="fas fa-sign-in-alt"></i> Logout</a>
                </c:when>
                <c:otherwise>
                    <a class="bar-item button" href="/login"><i class="fas fa-sign-out-alt"></i> Login</a>
                    <a class="bar-item button" href="/register"><i class="fas fa-plus-circle"></i> Register</a>
                </c:otherwise>
            </c:choose>
        </div>
    </div>
</div>
<div class="notification-box">
    <p id="notif" class="notification-text">
        <c:out value="${sessionScope.Error}"/>
        <c:remove var="Error" scope="session"/></p>
</div>
</body>