<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<html>
<t:header user="${user}" logged="${logged}"/>
<form action="/products/edit" method="post" enctype="multipart/form-data">
    Product Name:
    <br> <input type="text" name="name" value="<c:out value="${editItem.getName()}" />"><br>
    Quantity Available:
    <br> <input type="number" name="quantityAvailable" value="<c:out value="${editItem.getQuantity()}"/>"><br>
    Price:
    <br> <input type="number" step="0.01" name="price" value="<c:out value="${editItem.getPrice()}"/>"><br>
    Product Information:
    <br> <textarea name="productInformation" cols="50" rows="10"><c:out value="${editItem.getProductInformation()}"/></textarea>
	<br> Image: <input type="file" name="pic" accept="image/*" value="<c:out value="${editItem.getImageName()}"/>"><br>
	<input type="submit" name="button" value="Edit">
    <input type="submit" name="button" value="Back">
</form>
<t:footer/>
</html>
