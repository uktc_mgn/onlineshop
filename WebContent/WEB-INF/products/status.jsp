<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags"%>
<t:header user="${user}" logged="${logged}" />
<html>
<body>
    <c:forEach items="${purchases}" var="item">
        <h1>Purchase ID: ${item.getPurchaseId()}</h1>
        ${item.getPurchaseDate()}<br>
        <c:choose>
            <c:when test="${item.getPurchaseStatus() == 0}">
                Awaiting confirmation...
            </c:when>
            <c:when test="${item.getPurchaseStatus() == 1}">
                Confirmed.
            </c:when>
            <c:when test="${item.getPurchaseStatus() == 2}">
                Sent.
            </c:when>
            <c:when test="${item.getPurchaseStatus() == 3}">
                Completed.
            </c:when>
            <c:when test="${item.getPurchaseStatus() == 4}">
                Cancelled.
            </c:when>
            <c:when test="${item.getPurchaseStatus() == 5}">
                Refunded.
            </c:when>
            <c:when test="${item.getPurchaseStatus() == 6}">
                Lost in delivery.
            </c:when>
        </c:choose>
        <form action="/products/status" method="post">
            <input type="number" name="purchaseId" value="${item.getPurchaseId()}" hidden>
            <select name="status">
                <option value="1">Confirmed</option>
                <option value="2">Sent</option>
                <option value="3">Completed</option>
                <option value="4">Cancelled</option>
                <option value="5">Refunded</option>
                <option value="6">Lost in delivery</option>
            </select>
            <input type="submit" value="Update">
        </form>
    </c:forEach>
</body>
<t:footer/>
</html>
