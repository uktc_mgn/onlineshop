<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<html>
<t:header user="${user}" logged="${logged}"/>
<head>
    <script
            src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <link rel="stylesheet" href="ProductCss/indexStyle.css">
    <script>
        function incrementQuantity(id) {
            var quantity = document.getElementById("quantityOfCartProduct" + id).value;
            quantity++;
            if (quantity == 99) {
                return;
            }
            var s = document.getElementById("quantityOfCartProduct" + id);
            s.value = quantity;
        }

        function decrementQuantity(id) {
            var quantity = document.getElementById("quantityOfCartProduct" + id).value;
            if (quantity <= 1) {
                return;
            }
            quantity--;
            var s = document.getElementById("quantityOfCartProduct" + id);
            s.value = quantity;
        }

        function addToCart(id) {
            let quantity = document.getElementById("quantityOfCartProduct" + id).value;
            if (quantity === "" || quantity === "0") {
                quantity = 1;
            }
            var dataToSend = {
                quantityToCart: quantity,
                productId: id
            };
            $.ajax({
                url: '/products',
                data: dataToSend,
                type: 'POST',
                dataType: 'html',
                success: function (response) {
                    setTimeout(showNotification(response), 1);
                }
            });
            document.getElementById("quantityOfCartProduct" + id).value = 1;
        }

        function showNotification(error) {
            document.getElementById("notif").innerHTML = error;
            if (!(document.querySelector(".notification-box").classList.contains("show") ||
                document.querySelector(".notification-box").classList.contains("flip") ||
                document.querySelector(".notification-box").classList.contains("hide"))) {

                document.querySelector(".notification-box").classList.add("show");
                setTimeout(function () {
                    document.querySelector(".notification-box").classList.remove("show");
                    document.querySelector(".notification-box").classList.add("flip");
                    setTimeout(function () {
                        document.querySelector(".notification-text").classList.add("show");
                    }, 300);
                    setTimeout(function () {
                        document.querySelector(".notification-box").classList.remove("flip");
                        document.querySelector(".notification-box").classList.add("unflip");
                        setTimeout(function () {
                            document.querySelector(".notification-text").classList.remove("show");
                        }, 200);
                        setTimeout(function () {
                            document.querySelector(".notification-box").classList.remove("unflip");
                            document.querySelector(".notification-box").classList.add("hide");
                            setTimeout(function () {
                                document.querySelector(".notification-box").classList.remove("hide");
                            }, 750);
                        }, 750);
                    }, 4000);
                }, 750);
            }
        }
    </script>
</head>
<body>
<div class="flex-container">
    <c:forEach items="${products}"
               var="item">
        <div class="product-box row col-xs-12 col-sm-6 col-md-4 col-lg-3">
            <a class="image-link" href="/products/details?Id=${item.getId()}"><img alt="product" src="resources/images/${item.getImageName()}"></a>
            <h3>${item.getName()}</h3>
            <p>Quantity available:
                <c:choose>
                    <c:when test="${item.getQuantity() <= 0}">Restock!</c:when>
                    <c:otherwise>${item.getQuantity()}</c:otherwise>
                </c:choose>
            </p>
            <p>Price per product: ${item.getPrice()}$</p>
            <p><a href="/products/details?Id=${item.getId()}">more..</a></p>
            <c:if test="${user.getRole()>=1}">
                <p>
                    <a href="products/delete?Id=${item.getId()}">Delete</a>
                </p>
                <p>
                    <a href="products/edit?Id=${item.getId()}">Edit</a>
                </p>
            </c:if>
            <c:if test="${logged}">
                <p>
                <div id="quantity">
                    <i class="fas fa-minus" onclick="decrementQuantity(${item.getId()})"></i>
                    <input type="number" style="background-color: transparent" id="quantityOfCartProduct${item.getId()}"
                           value="1">
                    <i class="fas fa-plus" onclick="incrementQuantity(${item.getId()})"></i>
                </div>
                </p>
                <p>
                    <a onclick="addToCart(${item.getId()})">Add to cart</a>
                </p>
            </c:if>
        </div>
    </c:forEach>
</div>
<t:footer/>
</body>
</html>
