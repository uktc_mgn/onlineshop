
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags"%>
<html>
<t:header user="${user}" logged="${logged}" />
<form action="/products/create" method="post"
	enctype="multipart/form-data">
	Product Name: <br>
    <input type="text" name="name" value="<c:out value="${param.name}" />"><br> Quantity
	Available: <br>
    <input type="number" name="quantityAvailable" value="<c:out value="${param.quantityAvailable}"/>"><br>
	Price: <br>
    <input type="number" step="0.01" name="price" value="<c:out value="${param.price}"/>"><br> Product
	Information: <br>
    <textarea name="productInformation" cols="50" rows="10" value="<c:out value="${param.productInformation}"/>"></textarea>
	<br> Image:<br>
    <input type="file" name="pic" accept="image/*"><br>
	<input type="submit" name="submit">
</form>
<t:footer/>
</html>
