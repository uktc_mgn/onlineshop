<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<html>
<t:header user="${user}" logged="${logged}"/>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
</head>
<body>
<div class="detailsMargin" style="margin: 20px; margin-top: 0;">
    <img
            src="/resources/images/<c:out value="${detailsItem.getImageName()}"/>"
            width="150" height="auto">
    <br>
    <c:out value="${detailsItem.getName()}"/>
    <br>
    <c:out value="${detailsItem.getQuantity()}"/>
    <br>
    <c:out value="${detailsItem.getPrice()}"/>
    $
    <br>
    <c:out value="${detailsItem.getProductInformation()}"/>
    <br>
</div>
</body>
<t:footer/>
</html>