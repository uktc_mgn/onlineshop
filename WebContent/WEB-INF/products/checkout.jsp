<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags"%>
<html>
<head>
    <script
        src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
</head>
<t:header user="${user}" logged="${logged}" />
<body>
    <ul>
        <p style="width: 100%">
            Delivery address(Pre set to the account's address):
            <input type="text" name="deliveryAddress" value="<c:out value="${user.getAddress()}"/>" placeholder="Delivery address">
        </p>
        <br>
        <ui style="list-style-type:none;">
        <c:forEach items="${checkoutProducts}" var="cItem">
            <div class="outerDiv${cItem.getId()}" style="position: relative; float:left; display:inline-block; margin: 20px; margin-top: 0;">
            <li><a href="details?Id=${cItem.getId()}" style="text-decoration: none;">
                <img src="/resources/images/${cItem.getImageName()}" width="150" height="150">
                </a><br>${cItem.getName()}<br>
                        ${cItem.getPrice()*cItem.getQuantityInCart()}$
                    <br>
                        ${cItem.getQuantityInCart()}
                </li>
            </div>
        </c:forEach>
        </ui>
        <form action="/products/checkout" method="post">
            <input type="submit" value="Finish checking out" style="position: absolute; bottom: 10px; right: 50px; margin-right: -44.5px">
        </form>
        </ul>


    </form>
</body>
<t:footer/>
</html>