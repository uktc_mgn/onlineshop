<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags"%>
<html>
<t:header user="${user}" logged="${logged}" />
<p>Are you sure you want to delete:</p>
<img
	src="/resources/images/<c:out value="${deleteItem.getImageName()}"/>"
	width="150" height="auto">
<br>
<c:out value="${deleteItem.getName()}" />
<br>
<c:out value="${deleteItem.getQuantity()}" />
<br>
<c:out value="${deleteItem.getPrice()}" />
$
<br>
<c:out value="${deleteItem.getProductInformation()}" />
<br>
<form action="/products/delete" method="post">
	<input type="submit" name="button" value="Delete"> <input
		type="submit" name="button" value="Back">
</form
<t:footer/>
</html>
