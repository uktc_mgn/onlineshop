<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<t:header user="${user}" logged="${logged}"/>

<html>
<header>
    <link rel="stylesheet" href="/indexCss/homeStyle.css">
    <script>
        var slideIndex = 1;

        function showSlides(n) {
            let i;
            let slides = document.getElementsByClassName("slides");
            if (n > slides.length) {
                slideIndex = 1
            }
            if (n < 1) {
                slideIndex = slides.length
            }
            for (i = 0; i < slides.length; i++) {
                slides[i].style.display = "none";
            }
            slides[slideIndex - 1].style.display = "block";
        }

        setTimeout(function () {
            showSlides(1);
        }, 1)

        function plusSlides(n) {
            showSlides(slideIndex += n);
        }
    </script>
</header>
<body>

<div class="gallery">
    <c:forEach items="${products}" var="item">
        <div class="slides fade">
            <a href="/products/details?Id=${item.getId()}"><img src="resources/images/${item.getImageName()}"></a>
            <div class="text">${item.getName()}</div>
        </div>

    </c:forEach>
    <div class="controls">
        <a class="prev" onclick="plusSlides(-1)">&#10094;</a>
        <a class="next" onclick="plusSlides(1)">&#10095;</a>
    </div>
</div>
<t:footer/>
</body>
</html>
