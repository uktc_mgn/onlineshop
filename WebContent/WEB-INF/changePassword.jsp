
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags"%>
<t:header user="${user}" logged="true" />
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<h1>Change Password</h1>
	<c:out value="${error}" />

<form action="/changePassword" method="post">
New password:<br>
<input type="password" name="password"><br>
Confirm new password:<br>
<input type="password" name="confirmPassword"><br>
Old password:<br>
<input type="password" name="oldPassword"><br>
<br>
<input type="submit" value="Change Password">

</form>


</body>
<t:footer/>
</html>