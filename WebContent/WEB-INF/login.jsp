<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<html>
<t:header logged="false"/>
<link rel="stylesheet" href="/indexCss/loginStyle.css">

<script>
    function submit() {
        document.getElementById('loginForm').submit();
    }
</script>
<body>
<div class="form">
    <div class="innerForm">
        <form action="/login" method="post" id="loginForm">
            <h3>Sign in</h3>
            <div class="inputEmail">
                <input type="email" name="email" value="${email}" placeholder="Enter your email here..." required>
                <i class="fas fa-user"></i>
            </div>
            <div class="inputPassword">
                <input type="password" name="password" placeholder="Enter your password here..." required>
                <i class="fas fa-lock"></i>
            </div>
            <a onclick="submit()">Login</a>
        </form>
        <a href="/forgottenPassword">Forgotten Password?</a>
    </div>
</div>
<t:footer/>
</body>
</html>