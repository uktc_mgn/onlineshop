<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags"%>
<t:header user="${user}" logged="false" />
<html>
<body>
<h1>Reset Forgotten Password</h1>


<form action="/resetForgottenPassword" method="post">
    Enter verification code:<br>
    <input type="text" name="verificationCode"><br>
    Enter your new password:<br>
    <input type="password" name="newPassword"><br>
    Confirm your new password:<br>
    <input type="password" name="confirmNewPassword"><br><br>
    <input type="submit" value="Reset your password">
</form>

</body>
<t:footer/>
</html>
