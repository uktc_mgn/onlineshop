<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags"%>
<t:header user="${user}" logged="${logged}" />
<html>
<body>
	<p>
		Email:
		<c:out value="${user.getEmail()}" />
	</p>
	<p>
		Name:
		<c:out value="${user.getFirstName()}" />
		<c:out value="${user.getLastName()}" />
	</p>
	<p>
		Home town:
		<c:out value="${user.getTown()}" />
	</p>
	<p>
		Address:
		<c:out value="${user.getAddress()}" />
	</p>
	<p>
		Tel. num:
		<c:out value="${user.getTelNum()}" />
	</p>
	<a href="/profileEdit">Edit</a>
	<br>
	<a href="/changePassword">Change password</a>
	<br>
    <ul style="list-style-type: none;">
        <c:forEach var="purchase" items="${userPurchases}">
            <div class="outerDiv${purchase.getPurchaseId()}" style="position: relative; float:left; display:inline-block; margin: 20px; margin-top: 0">
                <h2> Purchase number: ${purchase.getPurchaseId()} </h2>
				<c:choose>
					<c:when test="${purchase.getPurchaseStatus() == 0}">
						Awaiting confirmation...
					</c:when>
					<c:when test="${purchase.getPurchaseStatus() == 1}">
						Confirmed.
					</c:when>
					<c:when test="${purchase.getPurchaseStatus() == 2}">
						Sent.
					</c:when>
					<c:when test="${purchase.getPurchaseStatus() == 3}">
						Completed.
					</c:when>
					<c:when test="${purchase.getPurchaseStatus() == 4}">
						Cancelled.
					</c:when>
					<c:when test="${purchase.getPurchaseStatus() == 5}">
						Refunded.
					</c:when>
					<c:when test="${purchase.getPurchaseStatus() == 6}">
						Lost in delivery.
					</c:when>
				</c:choose>
                <c:forEach var="purchasedCart" items="${purchasedProducts}">
                    <div class="innerDiv${purchasedCart.getId()}" >
                        <c:if test="${purchase.getPurchaseId() == purchasedCart.getPurchaseId()}">
                            <a href="/products/details?Id=${purchasedCart.getId()}" style="text-decoration: none;">
                                <img src="/resources/images/${purchasedCart.getImageName()}" width="150" height="150">
                            </a>
                            <br>
                            ${purchasedCart.getName()}
                            ${purchasedCart.getQuantityInCart()}<br>
                            ${purchasedCart.getQuantityInCart()*purchasedCart.getPrice()}$<br>

                        </c:if>
                    </div>
                </c:forEach>
            </div>
        </c:forEach>
    </ul>
</body>
<t:footer/>
</html>