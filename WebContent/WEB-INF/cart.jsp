<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<html>
<head>
    <script
            src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
</head>
<t:header user="${user}" logged="${logged}"/>
<body>
<ul style="list-style-type:none; margin: 0; padding: 0;"><c:forEach
        items="${productsCart}" var="item">
    <div class="outerDiv${item.getId()}"
         style="position: relative; float:left; display:inline-block; margin: 20px; margin-top: 0;">
        <c:if test="${item.getPurchaseId() == 0}">
            <li><a href="/products/details?Id=${item.getId()}" style="text-decoration: none;">
                <img src="/resources/images/${item.getImageName()}" width="150" height="150px">
            </a>
                <br>${item.getName()}<br>
                Total price: <fmt:formatNumber type ="NUMBER" minFractionDigits="2"
                                              value="${item.getPrice()*item.getQuantityInCart()}"/>$<br>
                    ${item.getQuantityInCart()}<br>
                <input type="number" id="quantityToRemove${item.getId()}" value="1" style="width: 80px;">
                <input type="button" onclick="removeFromCart(${item.getId()})" value="Remove">
            </li>
        </c:if>
    </div>
</c:forEach></ul>
<c:if test="${not empty productsCart}">
    <button style="position: absolute; bottom: 10px; right: 50px; margin-right: -44.5px"><a
            style="text-decoration: none;" href="/products/checkout">Checkout</a></button>
</c:if>
<c:if test="${empty productsCart}">
    <h1>Your cart is empty!</h1>
</c:if>
</body>
<script>

    function removeFromCart(id) {
        let quantity = document.getElementById("quantityToRemove" + id).value;
        if (quantity === "") {
            quantity = 1;
        }
        var dataToSend = {
            quantityToRemove: quantity,
            productId: id
        };
        $.ajax({
                url: '/cart',
                data: dataToSend,
                type: 'POST',
                dataType: 'html',
                success: function (response) {
                    setTimeout(showNotification(response), 1);
                }
            }
        );

        function showNotification(error) {
            document.getElementById("notif").innerHTML = error;
            if (!(document.querySelector(".notification-box").classList.contains("show") ||
                document.querySelector(".notification-box").classList.contains("flip") ||
                document.querySelector(".notification-box").classList.contains("hide"))) {

                document.querySelector(".notification-box").classList.add("show");
                setTimeout(function () {
                    document.querySelector(".notification-box").classList.remove("show");
                    document.querySelector(".notification-box").classList.add("flip");
                    setTimeout(function () {
                        document.querySelector(".notification-text").classList.add("show");
                    }, 300);
                    setTimeout(function () {
                        document.querySelector(".notification-box").classList.remove("flip");
                        document.querySelector(".notification-box").classList.add("unflip");
                        setTimeout(function () {
                            document.querySelector(".notification-text").classList.remove("show");
                        }, 200);
                        setTimeout(function () {
                            document.querySelector(".notification-box").classList.remove("unflip");
                            document.querySelector(".notification-box").classList.add("hide");
                            setTimeout(function () {
                                document.querySelector(".notification-box").classList.remove("hide");
                            }, 750);
                        }, 750);
                    }, 4000);
                }, 750);
            }
        }
    }

</script>
<t:footer/>
</html>
