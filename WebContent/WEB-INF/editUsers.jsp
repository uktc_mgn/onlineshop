<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags"%>
<html>
<t:header user="${user}" logged="true"/>
<head>
    <script type="text/javascript">
        function confSubmit(form) {
            if (confirm("Are you sure you want to Delete this user?")) {
                form.submit();
            }
        }
    </script>
</head>
<div class="all">
    <ui style="list-style-type:none"><c:forEach items="${users}"
                                                var="items">

        <c:if test="${user.getRole()==3}">
            <li>${items.getEmail()} ${items.getRole()}
                <form action="/users/delete" method="post">
                    <input type="hidden" name="deleteUserId" value="${items.getId()}">
                    <input type="button" onclick="confSubmit(this.form);" value="Delete">
                </form>
                <c:if test="${items.getRole()<3}">
                    <form action="/users/promote" method="post">
                        <input type="hidden" name="promoteUserId" value="${items.getId()}">
                        <input type="hidden" name="promoteCurrentRole" value="${items.getRole()}">
                        <input type="submit" value="Promote">
                    </form>
                </c:if>
                <c:if test="${items.getRole()>0}">
                    <form action="/users/demote" method="post">
                        <input type="hidden" name="demoteUserId" value="${items.getId()}">
                        <input type="hidden" name="demoteCurrentRole" value="${items.getRole()}">
                        <input type="submit" value="Demote">
                    </form>
                </c:if>
            </li>
            <br>
        </c:if>

        <c:if test="${user.getRole()==2}">
            <c:if test="${items.getRole()<=2}">
                <li>${items.getEmail()} ${items.getRole()}
                    <form action="/users/delete" method="post">
                        <input type="hidden" name="deleteUserId" value="${items.getId()}">
                        <input type="button" onclick="confSubmit(this.form);" value="Delete">
                    </form>
                    <c:if test="${items.getRole()<=1}">
                        <form action="/users/promote" method="post">
                            <input type="hidden" name="promoteUserId" value="${items.getId()}">
                            <input type="hidden" name="promoteCurrentRole" value="${items.getRole()}">
                            <input type="submit" value="Promote">
                        </form>
                    </c:if>
                    <c:if test="${items.getRole()>0}">
                        <form action="/users/demote" method="post">
                            <input type="hidden" name="demoteUserId" value="${items.getId()}">
                            <input type="hidden" name="demoteCurrentRole" value="${items.getRole()}">
                            <input type="submit" value="Demote">
                        </form>
                    </c:if>
                </li>
                <br>
            </c:if>
        </c:if>


    </c:forEach></ui>
</div>
<t:footer/>
</html>
