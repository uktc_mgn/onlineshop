<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<html>
<link rel="stylesheet" href="/indexCss/registerStyle.css">
<t:header logged="false"/>

    <div class="form">
        <div class="innerForm">
            <h3>Register</h3>
            <form action="/register" method="post" id="registerForm">
                <input type="text" name="firstName" value="${param.firstName}" placeholder="First name..." required>
                <input type="text" name="lastName" value="${param.lastName}" placeholder="Last name..." required>
                <input type="email" name="email" value="${param.email}" placeholder="E-mail..." required>
                <input type="password" name="password" placeholder="Password.." required>
                <input type="password" name="confirmPassword" placeholder="Confirm password..." required>
                <input type="text" name="town" value="${param.town}" placeholder="Town..." required>
                <input type="text" name="address" value="${param.address}" placeholder="Address..." required>
                <input type="text" name="telNum" value="${param.telNum}" placeholder="Phone number..." required>
                <a onclick="submit()"><p>Register</p></a>
            </form>
        </div>
    </div>
<script>
    function submit(){
        document.getElementById('registerForm').submit();
    }
</script>

<t:footer/>
</html>

