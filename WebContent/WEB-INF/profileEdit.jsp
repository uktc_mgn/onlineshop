<%--
  Created by IntelliJ IDEA.
  User: ZeáL
  Date: 15-May-18
  Time: 10:48
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags"%>
<t:header user="${user}" logged="true" />
<html>
<body>
	<form action="/profileEdit" method="post">
		Email: <br>
		<input type="email" name="email" value="<c:out value="${user.getEmail()}"/>"><br>
		First name: <br>
		<input type="text" name="firstName" value="<c:out value="${user.getFirstName()}"/>"><br>
		Last name: <br>
		<input type="text" name="lastName" value="<c:out value="${user.getLastName()}"/>"><br>
		Address: <br>
		<input type="text" name="address" value="<c:out value="${user.getAddress()}"/>"><br> Home
		Town: <br>
		<input type="text" name="town" value="<c:out value="${user.getTown()}"/>"><br>
		Tel. number:<br>
		<input type="text" name="telNum" value="<c:out value="${user.getTelNum()}"/>"><br>
		<input type="submit" value="Submit">
	</form>
</body>
<t:footer/>
</html>
