
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags"%>
<t:header logged="false" />
<html>
<body>

<h1>Reset password</h1>

    <form action="/forgottenPassword" method="post">
        Please enter your email address:<br>
        <input type="email"  name="userEmail">
        <input type="submit" value="Send email">

    </form>

</body>
<t:footer/>
</html>
