package Model;

import java.security.MessageDigest;
import java.security.SecureRandom;
import java.util.Random;


public class Password {
	
	private String password;
	private static final Random RANDOM= new SecureRandom();

	public Password(String password) {
		this.password = password;
	}
	public byte[] generateSalt() {
	    byte[] salt = new byte[16];
	    RANDOM.nextBytes(salt);
	    return salt;
	 }
	public 	String generateHash(String password) {
	    String encrypted = "";
	    try {
	        MessageDigest digest = MessageDigest.getInstance( "MD5" ); 
	        byte[] passwordBytes = password.getBytes( ); 

	        digest.reset( );
	        digest.update( passwordBytes );
	        byte[] message = digest.digest( );

	        StringBuffer hexString = new StringBuffer();
	        for ( int i=0; i < message.length; i++) 
	        {
	            hexString.append( Integer.toHexString(
	                0xFF & message[ i ] ) );
	        }
	        encrypted = hexString.toString();
	    }
	    catch( Exception e ) { }
	    return encrypted; 
	}

	}
	 
	 
	
