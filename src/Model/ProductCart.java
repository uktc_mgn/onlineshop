package Model;

public class ProductCart extends Product{

	private int quantityInCart;
	private int purchaseId;

	public int getPurchaseId() {
		return purchaseId;
	}

	public void setPurchaseId(int purchaseId) {
		this.purchaseId = purchaseId;
	}

	public ProductCart(int id, String name, int quantity, double price, String productInformation, String imageName, int quantityInCart, int purchaseId) {
		super(id, name, quantity, price, productInformation, imageName);
		this.quantityInCart = quantityInCart;
		this.purchaseId = purchaseId;
	}
	public int getQuantityInCart() {
		return quantityInCart;
	}
	public void setQuantityInCart(int quantityInCart) {
		this.quantityInCart = quantityInCart;
	}
	
	
}
