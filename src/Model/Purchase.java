package Model;

public class Purchase {
    private int purchaseId;
    private int purchaseStatus;
    private String deliveryAddress;
    private String purchaseDate;


    public Purchase(int purchaseId, int purchaseStatus, String deliveryAddress, String purchaseDate) {
        this.purchaseId = purchaseId;
        this.purchaseStatus = purchaseStatus;
        this.deliveryAddress = deliveryAddress;
        this.purchaseDate = purchaseDate;
    }

    public int getPurchaseId() {
        return purchaseId;
    }

    public void setPurchaseId(int purchaseId) {
        this.purchaseId = purchaseId;
    }

    public int getPurchaseStatus() {
        return purchaseStatus;
    }

    public void setPurchaseStatus(int purchaseStatus) {
        this.purchaseStatus = purchaseStatus;
    }

    public String getDeliveryAddress() {
        return deliveryAddress;
    }

    public void setDeliveryAddress(String deliveryAddress) {
        this.deliveryAddress = deliveryAddress;
    }

    public String getPurchaseDate() {
        return purchaseDate;
    }

    public void setPurchaseDate(String purchaseDate) {
        this.purchaseDate = purchaseDate;
    }
}
