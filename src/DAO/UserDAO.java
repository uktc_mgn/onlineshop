package DAO;

import Model.Password;
import Model.User;

import java.sql.*;

import java.util.ArrayList;

import Utilities.Config;
import Utilities.MySQL;



public class UserDAO {
	public static User getUser(String email, String password) throws SQLException {
		try {
			MySQL.connect();
			Connection connection = MySQL.getConnection();

			Password pass = new Password(password);
			String sql = "SELECT * FROM users WHERE email = ?";
			PreparedStatement statement = connection.prepareStatement(sql);
			statement.setString(1, email);
			ResultSet rs = statement.executeQuery();
			rs.next();
			String salt = rs.getString("salt");
			String saltedPass = salt + password;
			password = pass.generateHash(saltedPass);

			sql = "SELECT * FROM users WHERE email = ? AND password = ?";
			statement = connection.prepareStatement(sql);
			statement.setString(1, email);
			statement.setString(2, password);
			rs = statement.executeQuery();
			User user = null;
			if (rs != null && rs.next()) {
				user = new User(rs.getInt("id"), rs.getString("email"), rs.getString("password"),
						rs.getString("firstName"), rs.getString("lastName"), rs.getString("town"),
						rs.getString("address"), rs.getString("telNum"), rs.getInt("role"),
						rs.getString("salt"));
			}
			MySQL.closeConnection();
			return user;
		} catch (SQLException e) {
			MySQL.closeConnection();
			return null;
		}
	}

	//add verification code to DB and Current date
	public static void addVerificationCodeToDB(String verificationCode,String email)throws SQLException{

		try{
			MySQL.connect();
			Connection con=MySQL.getConnection();
			String sql ="SELECT * FROM users WHERE email=?";
			PreparedStatement prs=con.prepareStatement(sql);
			prs.setString(1,email);
			ResultSet rs=prs.executeQuery();
			rs.next();
			int userId=rs.getInt("id");



			sql="INSERT INTO user_password(user_id,verificationCode) values(?,?) ";

			prs=con.prepareStatement(sql);
			prs.setInt(1,userId);
			prs.setString(2,verificationCode);
			prs.executeUpdate();
			MySQL.closeConnection();

		}catch (SQLException e) {
			MySQL.closeConnection();

		}

	}
	//get user id by email
	public static int getUserIdByEmail(String email)throws SQLException{
		int user_id=0;
		try{
			MySQL.connect();
			Connection con=MySQL.getConnection();
			String sql="SELECT * FROM users WHERE email=?";
			PreparedStatement prs=con.prepareStatement(sql);
			prs.setString(1,email);
			ResultSet rs=prs.executeQuery();
			rs.next();
			user_id=rs.getInt("id");

			MySQL.closeConnection();
			return user_id;

		}catch (SQLException e){
			MySQL.closeConnection();
			return user_id;
		}
	}



	//check if the code is valid nad change password
	public static boolean checkIfTheVerificationCodeIsValid(int user_id,String verificationCode,String newPassword)throws SQLException{
		try{
			MySQL.connect();
			Connection con=MySQL.getConnection();
			String sql="SELECT * FROM user_password WHERE user_id=?";
			PreparedStatement prs=con.prepareStatement(sql);
			prs.setInt(1,user_id);
			ResultSet rs=prs.executeQuery();
            rs.next();
            Timestamp timestamp = rs.getTimestamp("creationDate");
            String codeDB = rs.getString("verificationCode");
			Timestamp timeCurrent = new Timestamp(System.currentTimeMillis());
			long diff = timeCurrent.getTime() - timestamp.getTime();

			if(diff <= 1200000){
                if(codeDB.equals(verificationCode)){
                   Password pass=new Password(newPassword);
                   sql = "SELECT * FROM users WHERE id = ?;";
                   prs=con.prepareStatement(sql);
                   prs.setInt(1,user_id);
                   rs=prs.executeQuery();
                   rs.next();
                   String salt=rs.getString("salt");
                   String newSaltedPass=salt+newPassword;
                   newPassword=pass.generateHash(newSaltedPass);

                    sql = "UPDATE users SET password = ? WHERE id= ?";
                    prs = con.prepareStatement(sql);
                    prs.setString(1, newPassword);
                    prs.setInt(2, user_id);
                    prs.executeUpdate();

                    sql="DELETE FROM user_password WHERE user_id=?";
                    prs=con.prepareStatement(sql);
                    prs.setInt(1,user_id);
                    prs.executeUpdate();


                    MySQL.closeConnection();
                    return true;

                }else{
			    MySQL.closeConnection();
                return false;
            }

            }
                MySQL.closeConnection();
                return false;


		}catch (SQLException e){
		    e.printStackTrace();
			MySQL.closeConnection();
			return false;
		}
	}


	//check for existing email
	public static boolean checkForExistingEmail(String emailToCheck)throws SQLException{

		try {
			MySQL.connect();
			Connection con=MySQL.getConnection();

			String sql="SELECT * FROM users WHERE email=?";
			PreparedStatement prs=con.prepareStatement(sql);
			prs.setString(1,emailToCheck);
			ResultSet rs = prs.executeQuery();
			rs.next();
			String emailDB=rs.getString("email");
			if(emailDB.equals(emailToCheck)){
				MySQL.closeConnection();
				return true;
			}else{
				MySQL.closeConnection();
				return false;
			}


		}catch (SQLException e){
			MySQL.closeConnection();
			return false;
		}

	}


	//Delete user
	public static boolean deleteUser(int user_id)throws SQLException{
		try{
			MySQL.connect();
			Connection con =MySQL.getConnection();

			String sql="DELETE FROM users WHERE id=?";
			PreparedStatement prs =con.prepareStatement(sql);
			prs.setInt(1,user_id);
			prs.executeUpdate();
			MySQL.closeConnection();
			return true;

		}catch (SQLException e) {
			MySQL.closeConnection();
			e.printStackTrace();
			return false;
		}
	}

	//Promote user
	public  static  boolean promoteUser(int user_id,int role)throws SQLException{
		try{
			MySQL.connect();
			Connection con =MySQL.getConnection();

			role = role +1;

			String sql="UPDATE users SET role=? WHERE id=?";
			PreparedStatement prs =con.prepareStatement(sql);
			prs.setInt(1,role);
			prs.setInt(2,user_id);
			prs.executeUpdate();
			MySQL.closeConnection();
			return true;

		}catch (SQLException e) {
			MySQL.closeConnection();
			e.printStackTrace();
			return false;
		}
	}

	//Demote user
	public static boolean demoteUser(int user_id,int role)throws SQLException{
		try{
			MySQL.connect();
			Connection con =MySQL.getConnection();

			role = role - 1;
			if(role<0){
				return false;
			}
			String sql="UPDATE users SET role=? WHERE id=?";
			PreparedStatement prs =con.prepareStatement(sql);
			prs.setInt(1,role);
			prs.setInt(2,user_id);
			prs.executeUpdate();
			MySQL.closeConnection();
			return true;

		}catch (SQLException e) {
			MySQL.closeConnection();
			e.printStackTrace();
			return false;
		}
	}


	// password change
	public static String changePass(int user_id, String password, String oldPassword) throws SQLException {
		try {
			MySQL.connect();
			Connection connection = MySQL.getConnection();

			Password pass1 = new Password(oldPassword);
			String sql = "SELECT * FROM users WHERE id = ?;";
			PreparedStatement prs = connection.prepareStatement(sql);
			prs.setInt(1, user_id);
			ResultSet rs = prs.executeQuery();
			rs.next();
			String oldPassDB = rs.getString("password");

			rs = prs.executeQuery();
			rs.next();
			String salt = rs.getString("salt");
			String saltedPass = salt + oldPassword;
			oldPassword = pass1.generateHash(saltedPass);

			if (oldPassDB.equals(oldPassword)) {
				Password pass = new Password(password);
				rs = prs.executeQuery();
				rs.next();
				salt = rs.getString("salt");
				saltedPass = salt + password;
				password = pass.generateHash(saltedPass);
				if (password.equals(oldPassword)) {
					MySQL.closeConnection();
					return Config.NEW_SAME_AS_OLD;
				} else {
					sql = "UPDATE users SET password = ? WHERE id= ?";
					prs = connection.prepareStatement(sql);
					prs.setString(1, password);
					prs.setInt(2, user_id);
					prs.executeUpdate();
					MySQL.closeConnection();
					return Config.PASSWORD_CHANGED;
				}
			} else {
				MySQL.closeConnection();
				return Config.INCORRECT_OLD_PASSWORD;
			}

		} catch (SQLException e) {
			MySQL.closeConnection();
			e.printStackTrace();
			return Config.SOMETHING_WRONG;
		}

	}

	public static boolean createUser(String email, String password, String firstName, String lastName, String address,
			String town, String telNum) throws SQLException {
		try {
			MySQL.connect();
			Connection connection = MySQL.getConnection();
			Password pass = new Password(password);
			byte[] saltByte = pass.generateSalt();
			String salt = saltByte.toString();
			String saltedPass = salt + password;
			password = pass.generateHash(saltedPass);

			String sql = "INSERT INTO users(email, password, firstName, lastName, address, town, telNum, salt) VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
			PreparedStatement statement = connection.prepareStatement(sql);
			statement.setString(1, email);
			statement.setString(2, password);
			statement.setString(3, firstName);
			statement.setString(4, lastName);
			statement.setString(5, address);
			statement.setString(6, town);
			statement.setString(7, telNum);
			statement.setString(8, salt);
			statement.executeUpdate();
			MySQL.closeConnection();
			return true;
		} catch (SQLException e) {
			MySQL.closeConnection();
			return false;
		}
	}

	public static User getUserById(int id) throws SQLException {
		try {
			MySQL.connect();
			Connection connection = MySQL.getConnection();
			String sql = "SELECT * FROM users WHERE id = ?";
			PreparedStatement statement = connection.prepareStatement(sql);
			statement.setInt(1, id);
			ResultSet rs = statement.executeQuery();
			if (rs != null && rs.next()) {
				User user = new User(rs.getInt("id"), rs.getString("email"), rs.getString("password"),
						rs.getString("firstName"), rs.getString("lastName"), rs.getString("town"),
						rs.getString("address"), rs.getString("telNum"), rs.getInt("role"),
						rs.getString("salt"));
				MySQL.closeConnection();
				return user;
			}
		} catch (SQLException e) {
			e.printStackTrace();
			MySQL.closeConnection();
		}
		return null;

	}

	public static boolean updateUserInfo(String email, String firstName, String lastName, String address, String town,
			String telNum, int userId) throws SQLException {
		try {
			MySQL.connect();
			Connection connection = MySQL.getConnection();
			String sql = "UPDATE users SET email = ?, firstName = ?, lastName = ?, address = ?, town = ?, telNum = ? WHERE id=?";
			PreparedStatement prs = connection.prepareStatement(sql);
			prs.setString(1, email);
			prs.setString(2, firstName);
			prs.setString(3, lastName);
			prs.setString(4, address);
			prs.setString(5, town);
			prs.setString(6, telNum);
			prs.setInt(7, userId);
			prs.executeUpdate();
			return true;
		} catch (SQLException e) {
			e.printStackTrace();
			MySQL.closeConnection();
			return false;
		}
	}


	public static ArrayList<User> getAllUsers()throws SQLException{
		try {
			MySQL.connect();
			Connection connection = MySQL.getConnection();
			String sql = "SELECT * FROM users";
			Statement statement = connection.prepareStatement(sql);
			ResultSet rs = statement.executeQuery(sql);
			ArrayList<User> users = new ArrayList<>();
			if (rs != null) {
				while (rs.next()) {
					User user = new User(rs.getInt("id"), rs.getString("email"),
							rs.getString("password"), rs.getString("firstName"), rs.getString("lastName"),
							rs.getString("address"),rs.getString("town"),rs.getString("telNum"),rs.getInt("role"),rs.getString("salt"));
					users.add(user);
				}
			}
			MySQL.closeConnection();
			return users;
		} catch (SQLException e) {
			e.printStackTrace();
			MySQL.closeConnection();
			return null;
		}
	}
}
