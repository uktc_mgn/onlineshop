package DAO;

import Model.Product;
import Model.ProductCart;
import Model.Purchase;
import Model.User;
import Utilities.MySQL;

import java.sql.*;
import java.util.ArrayList;

public class ProductDAO {
    public static boolean createProduct(String name, int quantityAvailable, double price, String productInformation,
                                        String imageName) throws SQLException {
        try {
            MySQL.connect();
            Connection connection = MySQL.getConnection();
            String sql = "INSERT INTO products(productName, quantityAvailable, price, productInformation, imageName) VALUES (?, ?, ?, ? ,?)";
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setString(1, name);
            statement.setInt(2, quantityAvailable);
            statement.setDouble(3, price);
            statement.setString(4, productInformation);
            statement.setString(5, imageName);
            statement.executeUpdate();
            MySQL.closeConnection();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            MySQL.closeConnection();
            return false;
        }
    }

    public static ArrayList<Product> getProducts() throws SQLException {
        try {
            MySQL.connect();
            Connection connection = MySQL.getConnection();
            String sql = "SELECT * FROM products";
            Statement statement = connection.prepareStatement(sql);
            ResultSet rs = statement.executeQuery(sql);
            ArrayList<Product> products = new ArrayList<>();
            if (rs != null) {
                while (rs.next()) {
                    Product product = new Product(rs.getInt("id"), rs.getString("productName"),
                            rs.getInt("quantityAvailable"), rs.getDouble("price"), rs.getString("productInformation"),
                            rs.getString("imageName"));
                    products.add(product);
                }
            }
            MySQL.closeConnection();
            return products;
        } catch (SQLException e) {
            e.printStackTrace();
            MySQL.closeConnection();
            return null;
        }
    }

    public static Product getProductById(int id) throws SQLException {
        try {
            MySQL.connect();
            Connection connection = MySQL.getConnection();
            String sql = "SELECT * FROM products WHERE id = ?";
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setInt(1, id);
            ResultSet rs = statement.executeQuery();
            Product product = null;
            if (rs != null && rs.next()) {
                product = new Product(rs.getInt("id"), rs.getString("productName"), rs.getInt("quantityAvailable"),
                        rs.getDouble("price"), rs.getString("productInformation"), rs.getString("imageName"));
            }
            MySQL.closeConnection();
            return product;
        } catch (SQLException e) {
            e.printStackTrace();
            MySQL.closeConnection();
            return null;
        }
    }

    public static void deleteProductById(int id) throws SQLException {
        try {
            MySQL.connect();
            Connection connection = MySQL.getConnection();
            String sql = "DELETE FROM products WHERE id = ?";
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setInt(1, id);
            statement.execute();
            MySQL.closeConnection();
        } catch (SQLException e) {
            e.printStackTrace();
            MySQL.closeConnection();
        }
    }

    public static void editProduct(Product product) throws SQLException {
        try {
            MySQL.connect();
            Connection connection = MySQL.getConnection();
            String sql = "UPDATE products SET productName = ?, quantityAvailable = ?, price= ?, productInformation= ?, imageName = ? WHERE id = ?";
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setString(1, product.getName());
            statement.setInt(2, product.getQuantity());
            statement.setDouble(3, product.getPrice());
            statement.setString(4, product.getProductInformation());
            statement.setString(5, product.getImageName());
            statement.setInt(6, product.getId());
            statement.executeUpdate();
            MySQL.closeConnection();
        } catch (SQLException e) {
            e.printStackTrace();
            MySQL.closeConnection();
        }
    }

    public static ArrayList<ProductCart> getCartProducts(User user) throws SQLException {
        try {
            MySQL.connect();
            Connection connection = MySQL.getConnection();
            String sql = "SELECT p.id, p.productName, p.quantityAvailable, p.price," +
                    " p.productInformation, p.imageName, products_cart.quantityOfCartProduct, products_cart.purchase_id" +
                    " FROM products as p JOIN products_cart ON p.id = products_cart.product_id JOIN users ON" +
                    " products_cart.user_id = users.id WHERE users.id = ? AND purchase_id = 0";
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setInt(1, user.getId());
            ResultSet rs = statement.executeQuery();
            ArrayList<ProductCart> cartProducts = new ArrayList<>();
            if (rs != null) {
                while (rs.next()) {
                    ProductCart product = new ProductCart(rs.getInt("id"), rs.getString("productName"),
                            rs.getInt("quantityAvailable"), rs.getDouble("price"), rs.getString("productInformation"),
                            rs.getString("imageName"), rs.getInt("quantityOfCartProduct"), rs.getInt("purchase_id"));
                    cartProducts.add(product);
                }
            }
            MySQL.closeConnection();
            return cartProducts;
        } catch (SQLException e) {
            e.printStackTrace();
            MySQL.closeConnection();
            return null;
        }
    }

    public static boolean addToCartProduct(int productId, User user, int quantityOfCartProduct) throws SQLException {
        try {
            ArrayList<ProductCart> cartProds = ProductDAO.getCartProducts(user);
            if (!checkQuantityInDB(getProductById(productId), quantityOfCartProduct)) {
                return false;
            }
            if (cartProds.isEmpty()) {
                MySQL.connect();
                Connection connection = MySQL.getConnection();
                String sql = "INSERT INTO products_cart(product_id, user_id, quantityOfCartProduct) VALUES(?, ?, ?)";
                PreparedStatement statement = connection.prepareStatement(sql);
                statement.setInt(1, productId);
                statement.setInt(2, user.getId());
                statement.setInt(3, quantityOfCartProduct);
                statement.executeUpdate();
                updateQuantity(getProductById(productId), quantityOfCartProduct);
                MySQL.closeConnection();
                return true;
            }
            int cartProductId = -1;
            for (int j = 0; j < cartProds.size(); j++) {
                if (cartProds.get(j).getId() == productId) {
                    cartProductId = j;
                    break;
                }
            }
            if (cartProductId != -1) {
                int quantity = ProductDAO.getQuantityOfCartProducts(user, productId);
                MySQL.connect();
                Connection connection = MySQL.getConnection();
                String sql = "UPDATE products_cart SET quantityOfCartProduct = ? WHERE product_id = ? AND user_id=? AND purchase_id = 0";
                PreparedStatement statement = connection.prepareStatement(sql);
                statement.setInt(1, quantity + quantityOfCartProduct);
                statement.setInt(2, cartProds.get(cartProductId).getId());
                statement.setInt(3, user.getId());
                statement.executeUpdate();
                updateQuantity(getProductById(productId), quantityOfCartProduct);
                MySQL.closeConnection();
                return true;
            } else {
                MySQL.connect();
                Connection connection = MySQL.getConnection();
                String sql = "INSERT INTO products_cart(product_id, user_id, quantityOfCartProduct) VALUES(?, ?, ?)";
                PreparedStatement statement = connection.prepareStatement(sql);
                statement.setInt(1, productId);
                statement.setInt(2, user.getId());
                statement.setInt(3, quantityOfCartProduct);
                statement.executeUpdate();
                updateQuantity(getProductById(productId), quantityOfCartProduct);
                MySQL.closeConnection();
                return true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
            MySQL.closeConnection();
            return false;
        }
    }

    public static int getQuantityOfCartProducts(User user, int productId) throws SQLException {
        try {
            MySQL.connect();
            Connection connection = MySQL.getConnection();
            String sql = "SELECT * FROM products_cart WHERE user_id = ? AND product_id = ? AND purchase_id = 0";
            PreparedStatement prs = connection.prepareStatement(sql);
            prs.setInt(1, user.getId());
            prs.setInt(2, productId);
            ResultSet rs = prs.executeQuery();
            rs.next();
            int quantity = rs.getInt("quantityOfCartProduct");
            MySQL.closeConnection();
            return quantity;
        } catch (SQLException e) {
            e.printStackTrace();
            MySQL.closeConnection();
            return 0;
        }

    }

    public static boolean removeFromCart(User user, int productId, int quantityToRemove) {
        try {
            int quantityInDb = ProductDAO.getQuantityOfCartProducts(user, productId);
            MySQL.connect();
            Connection connection = MySQL.getConnection();
            if (quantityInDb - quantityToRemove <= 0) {
                quantityToRemove = quantityInDb;
                String sql = "DELETE FROM products_cart WHERE product_id = ? AND user_id = ? AND purchase_id = 0";
                PreparedStatement prs = connection.prepareStatement(sql);
                prs.setInt(1, productId);
                prs.setInt(2, user.getId());
                prs.executeUpdate();
                updateQuantity(getProductById(productId), -quantityToRemove);
                MySQL.closeConnection();
                return true;
            } else {
                String sql = "UPDATE products_cart SET quantityOfCartProduct = ? WHERE product_id = ? AND user_id = ? AND purchase_id = 0";
                PreparedStatement prs = connection.prepareStatement(sql);
                prs.setInt(1, quantityInDb - quantityToRemove);
                prs.setInt(2, productId);
                prs.setInt(3, user.getId());
                prs.executeUpdate();
                updateQuantity(getProductById(productId), -quantityToRemove);
                MySQL.closeConnection();
                return true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }


    }

    public static boolean addPurchase(User user) throws SQLException {
        try {
            MySQL.connect();
            Connection connection = MySQL.getConnection();
            String sql = "INSERT INTO user_purchase(user_id, deliveryAddress) values(?, ?)";
            PreparedStatement prs = connection.prepareStatement(sql);
            prs.setInt(1, user.getId());
            prs.setString(2, user.getAddress());
            prs.executeUpdate();
            prs.clearParameters();
            sql = "SELECT * FROM user_purchase WHERE user_id = ? ORDER BY id DESC LIMIT 1";
            prs = connection.prepareStatement(sql);
            prs.setInt(1, user.getId());
            ResultSet rs = prs.executeQuery();
            rs.next();
            int purchaseId = rs.getInt("id");
            sql = "UPDATE products_cart SET purchase_id = ? WHERE user_id = ? AND purchase_id = 0";
            prs = connection.prepareStatement(sql);
            prs.setInt(1, purchaseId);
            prs.setInt(2, user.getId());
            prs.executeUpdate();
            MySQL.closeConnection();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            MySQL.closeConnection();
            return false;
        }


    }

    public static ArrayList<Purchase> getUserPurchases(User user) throws SQLException {
        try {
            MySQL.connect();
            Connection connection = MySQL.getConnection();
            ArrayList<Purchase> userPurchases = new ArrayList<>();
            String sql = "SELECT * FROM user_purchase WHERE user_id = ?";
            PreparedStatement prs = connection.prepareStatement(sql);
            prs.setInt(1, user.getId());
            ResultSet rs = prs.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    Purchase purchase = new Purchase(rs.getInt("id"),
                            rs.getInt("purchaseStatus"),
                            rs.getString("deliveryAddress"),
                            rs.getString("purchaseDate")
                    );
                    userPurchases.add(purchase);
                }
            }
            MySQL.closeConnection();
            return userPurchases;
        } catch (SQLException e) {
            e.printStackTrace();
            MySQL.closeConnection();
            return null;
        }
    }

    public static ArrayList<ProductCart> getPurchasedCartProducts(User user) throws SQLException {
        try {
            MySQL.connect();
            Connection connection = MySQL.getConnection();
            String sql = "SELECT p.id, p.productName, p.quantityAvailable, p.price," +
                    " p.productInformation, p.imageName, products_cart.quantityOfCartProduct, products_cart.purchase_id" +
                    " FROM products as p JOIN products_cart ON p.id = products_cart.product_id JOIN users ON" +
                    " products_cart.user_id = users.id WHERE users.id = ? AND products_cart.purchase_id != 0";
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setInt(1, user.getId());
            ResultSet rs = statement.executeQuery();
            ArrayList<ProductCart> purchasedCartProducts = new ArrayList<>();
            if (rs != null) {
                while (rs.next()) {
                    ProductCart product = new ProductCart(rs.getInt("id"), rs.getString("productName"),
                            rs.getInt("quantityAvailable"), rs.getDouble("price"), rs.getString("productInformation"),
                            rs.getString("imageName"), rs.getInt("quantityOfCartProduct"), rs.getInt("purchase_id"));
                    purchasedCartProducts.add(product);
                }
            }

            MySQL.closeConnection();
            return purchasedCartProducts;
        } catch (SQLException e) {
            e.printStackTrace();
            MySQL.closeConnection();
            return null;
        }


    }

    public static boolean updateQuantity(Product product, int quantityToRemove) throws SQLException {
        try {
            MySQL.connect();
            Connection connection = MySQL.getConnection();
            String sql = "SELECT * FROM products WHERE id = ?";
            PreparedStatement prs = connection.prepareStatement(sql);
            prs.setInt(1, product.getId());
            ResultSet rs = prs.executeQuery();
            rs.next();
            int quantityInDB = rs.getInt("quantityAvailable");
            prs.clearParameters();
            sql = "UPDATE products SET quantityAvailable = ? WHERE id = ?";
            prs = connection.prepareStatement(sql);
            prs.setInt(1, quantityInDB - quantityToRemove);
            prs.setInt(2, product.getId());
            prs.executeUpdate();
            prs.clearParameters();
            MySQL.closeConnection();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            MySQL.closeConnection();
            return false;
        }

    }

    public static boolean checkQuantityInDB(Product product, int quantity) throws SQLException {
        try {
            MySQL.connect();
            Connection connection = MySQL.getConnection();
            String sql = "SELECT * FROM products WHERE id = ?";
            PreparedStatement prs = connection.prepareStatement(sql);
            prs.setInt(1, product.getId());
            ResultSet rs = prs.executeQuery();
            rs.next();
            prs.clearParameters();
            int quantityInDB = rs.getInt("quantityAvailable");
            if (quantityInDB >= quantity) {
                MySQL.closeConnection();
                return true;
            } else {
                MySQL.closeConnection();
                return false;
            }

        } catch (SQLException e) {
            e.printStackTrace();
            MySQL.closeConnection();
            return false;
        }

    }
    public static ArrayList<Purchase> getAllPurchases() throws SQLException {
        try{
            MySQL.connect();
            Connection connection = MySQL.getConnection();
            String sql = "SELECT * FROM user_purchase";
            PreparedStatement prs = connection.prepareStatement(sql);
            ResultSet rs = prs.executeQuery();
            ArrayList<Purchase> purchases = new ArrayList<>();
            while(rs.next()){
                if(rs != null){
                    Purchase purchase = new Purchase(rs.getInt("id"),
                            rs.getInt("purchaseStatus"),
                            rs.getString("deliveryAddress"),
                            rs.getString("purchaseDate"));
                    purchases.add(purchase);
                }
            }
            MySQL.closeConnection();
            return purchases;

        }catch(SQLException e){
            e.printStackTrace();
            MySQL.closeConnection();
            return null;
        }
    }
    public static boolean updateStatus(int purchaseId, int status) throws SQLException{
        try{
            MySQL.connect();
            Connection connection = MySQL.getConnection();
            String sql = "UPDATE user_purchase SET purchaseStatus = ? WHERE id = ?";
            PreparedStatement prs = connection.prepareStatement(sql);
            prs.setInt(1, status);
            prs.setInt(2, purchaseId);
            prs.executeUpdate();
            MySQL.closeConnection();
            return true;
        }catch(SQLException e){
            MySQL.closeConnection();
            e.printStackTrace();
            return false;
        }
    }

}
