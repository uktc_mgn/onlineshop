package Controller;

import Model.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.io.IOException;

@WebServlet(name = "Logout", urlPatterns = { "/logout" })
public class Logout extends HttpServlet {

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        HttpSession session = request.getSession(false);
        if(session != null) {
            User user = (User) session.getAttribute("User");
            if (user != null) {
                session.removeAttribute("User");
                session.invalidate();
                response.sendRedirect("/");
                return;
            }
        }
        response.sendRedirect("/login");
    }
}
