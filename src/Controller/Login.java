package Controller;

import DAO.UserDAO;
import Model.User;
import Utilities.Config;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


import java.io.IOException;
import java.sql.SQLException;

@WebServlet(name = "Login", urlPatterns = {"/login"})
public class Login extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String email = request.getParameter("email");
        String password = request.getParameter("password");
        String remember = request.getParameter("remember");
        HttpSession session = request.getSession();
        if (!password.matches("^(?=.*[\\S])(?=.*[0-9]).+")) {
            session.setAttribute("Error", Config.INCORRECT_PASSWORD);
            response.sendRedirect("/login");
            return;
        }
        if (password.length() > 30) {
            session.setAttribute("Error", Config.INCORRECT_PASSWORD);
            response.sendRedirect("/login");
            return;
        }
        User user = null;
        try {
            user = UserDAO.getUser(email, password);
        } catch (SQLException ignored) {
        }
        if (user == null) {
            session.setAttribute("Error", Config.INCORRECT_PASSWORD);
            response.sendRedirect("/login");
        } else {
            session.setAttribute("User", user);
            if (remember != null) {
                Cookie[] cookies = {new Cookie("name", email), new Cookie("password", password)};
                for (Cookie cooky : cookies) {
                    response.addCookie(cooky);
                }
            }
            response.sendRedirect("/");
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession(false);
        request.setAttribute("title", Config.TITLE_LOGIN);
        if (session != null) {
            String email = (String) session.getAttribute("email");
            if (email != null) {
                request.setAttribute("email", email);
                session.invalidate();
                request.setAttribute("logged", false);
                request.getRequestDispatcher("/WEB-INF/login.jsp").forward(request, response);
                return;
            }
            User user = (User) session.getAttribute("User");
            if (user != null) {
                response.sendRedirect("/");
                return;
            }
        }
        request.setAttribute("logged", false);
        request.getRequestDispatcher("/WEB-INF/login.jsp").forward(request, response);
    }
}
