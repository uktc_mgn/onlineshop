package Controller.Products;

import Utilities.Config;
import Utilities.FileEditing;
import DAO.ProductDAO;
import Model.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.io.IOException;
import java.sql.SQLException;

@WebServlet(name = "/Products/Create", urlPatterns = {"/products/create"})
@MultipartConfig
public class Create extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String name = request.getParameter("name");
        int quantityAvailable = Integer.parseInt(request.getParameter("quantityAvailable"));
        double price = Double.parseDouble(request.getParameter("price"));
        String productInformation = request.getParameter("productInformation");
        HttpSession session = request.getSession();
        try {
            if (ProductDAO.createProduct(name, quantityAvailable, price, productInformation, FileEditing.createFile(request))) {
                request.removeAttribute("name");
                request.removeAttribute("quantityAvailable");
                request.removeAttribute("price");
                request.removeAttribute("productInformation");
                request.removeAttribute("pic");
                session.setAttribute("Error", Config.PRODUCT_CREATED);
            } else {
                session.setAttribute("Error", Config.SOMETHING_WRONG);
            }
            response.sendRedirect("/products");
        } catch (SQLException ignored) {
        }

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession(false);
        request.setAttribute("title", Config.TITLE_CREATE_PRODUCT);
        if (session != null) {
            User user = (User) session.getAttribute("User");
            if (user != null) {
                if (user.getRole() >= 1) {
                    request.setAttribute("user", user);
                    request.setAttribute("logged", true);
                    request.getRequestDispatcher("/WEB-INF/products/create.jsp").forward(request, response);
                    return;
                }
            }
        }
        response.sendRedirect("/products");
    }
}
