package Controller.Products;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import DAO.ProductDAO;
import Model.Product;
import Model.User;

@WebServlet(name = "/Products/Details", urlPatterns = {"/products/details"})
public class Details extends HttpServlet {


    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession(false);
        if (session != null) {
            User user = (User) session.getAttribute("User");
            if (user != null) {
                request.setAttribute("user", user);
                request.setAttribute("logged", true);
            }
        }
        try {
            if (request.getParameter("Id") == null) {
                request.getRequestDispatcher("/WEB-INF/products/details.jsp").forward(request, response);
                return;
            }
            int id = Integer.parseInt(request.getParameter("Id"));
            Product product = ProductDAO.getProductById(id);
            if (product == null) {
                response.sendRedirect("/Products");
                return;
            }
            request.setAttribute("title", product.getName());
            request.setAttribute("detailsItem", product);
            request.getRequestDispatcher("/WEB-INF/products/details.jsp").forward(request, response);
            return;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        response.sendRedirect("/products");
    }

}
