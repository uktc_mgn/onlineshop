package Controller.Products;

import Utilities.Config;
import Utilities.FileEditing;
import DAO.ProductDAO;
import Model.Product;
import Model.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.SQLException;

@WebServlet(name = "/Products/Edit", urlPatterns = { "/products/edit" })
@MultipartConfig
public class Edit extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        if (request.getParameter("button").equals("Edit")) {
            HttpSession session = request.getSession(false);
            Product idProd = (Product) session.getAttribute("editItem");
            int id = idProd.getId();
            String name = request.getParameter("name");
            int quantityAvailable = Integer.parseInt(request.getParameter("quantityAvailable"));
            double price = Double.parseDouble(request.getParameter("price"));
            String productInformation = (String) request.getParameter("productInformation");
            Product product = new Product(id, name, quantityAvailable, price, productInformation, null);
            try {
                Product dbProduct = ProductDAO.getProductById(id);
                if (request.getPart("pic").getSize() == 0) {
                    product.setImageName(dbProduct.getImageName());
                } else {
                    FileEditing.deleteFile(request, dbProduct);
                    product.setImageName(FileEditing.createFile(request));
                }
                ProductDAO.editProduct(product);
                session.setAttribute("Error", Config.PRODUCT_EDITED);
                response.sendRedirect("/products");
                return;
            } catch (SQLException | ServletException e) {
                e.printStackTrace();
            }
        }
        response.sendRedirect("/products");
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession(false);
        request.setAttribute("title", Config.TITLE_EDIT_PRODUCT);
        if (session != null) {
            User user = (User) session.getAttribute("User");
            if (user != null) {
                if(user.getRole()>=1) {
                    try {
                        request.setAttribute("user", user);
                        request.setAttribute("logged", true);
                        if(request.getParameter("Id") == null){
                            request.getRequestDispatcher("/WEB-INF/products/edit.jsp").forward(request, response);
                            return;
                        }
                        int id = Integer.parseInt(request.getParameter("Id"));
                        Product product = ProductDAO.getProductById(id);
                        if (product != null) {
                            session.setAttribute("editItem", product);
                            response.sendRedirect("/products/edit");
                            return;
                        }
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        response.sendRedirect("/products");
    }
}
