package Controller.Products;

import DAO.ProductDAO;
import Model.Product;
import Model.User;
import Utilities.Config;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

@WebServlet(name = "/Products", urlPatterns = { "/products" })
public class Index extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	try {
    		HttpSession session = request.getSession(false);
    		User user = (User)session.getAttribute("User");
            if (ProductDAO.addToCartProduct(Integer.parseInt(request.getParameter("productId")),
                    user, Integer.parseInt(request.getParameter("quantityToCart")))) {
                response.setContentType("text/plain");
                response.setCharacterEncoding("UTF-8");
                response.getWriter().write(Config.PRODUCT_ADDED);
            }else{
                response.setContentType("text/plain");
                response.setCharacterEncoding("UTF-8");
                response.getWriter().write(Config.TOO_MUCH_QUANTITY);

            }
		} catch (NumberFormatException | SQLException e) {
			e.printStackTrace();
		}
    	    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            request.setAttribute("title", Config.TITLE_PRODUCTS);
            HttpSession session = request.getSession(false);
            ArrayList<Product> products = ProductDAO.getProducts();
            request.setAttribute("products", products);
            if(session != null) {
                User user = (User) session.getAttribute("User");
                if (user != null) {
                    request.setAttribute("user", user);
                    request.setAttribute("logged", true);
                    request.getRequestDispatcher("/WEB-INF/products/index.jsp").forward(request, response);
                    return;
                }
            }
            request.setAttribute("logged", false);
            request.getRequestDispatcher("/WEB-INF/products/index.jsp").forward(request, response);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    }
