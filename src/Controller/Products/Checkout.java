package Controller.Products;

import DAO.ProductDAO;
import Model.ProductCart;
import Model.User;
import Utilities.Config;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

@WebServlet(name = "/Products/Checkout", urlPatterns = {"/products/checkout"})
public class Checkout extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        HttpSession session = request.getSession(false);
        try {
            User user = (User) session.getAttribute("User");
            ProductDAO.addPurchase(user);
            session.setAttribute("Error", Config.PURCHASE_FINISHED);
        } catch (SQLException ignored) {
            session.setAttribute("Error", Config.SOMETHING_WRONG);
        } finally {
            response.sendRedirect("/");
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            HttpSession session = request.getSession(false);
            request.setAttribute("title", Config.TITLE_CHECKOUT);
            if (session != null) {
                User user = (User) session.getAttribute("User");
                if (user != null) {
                    request.setAttribute("user", user);
                    request.setAttribute("logged", true);
                    ArrayList<ProductCart> checkoutProducts = ProductDAO.getCartProducts(user);
                    if (checkoutProducts.size() == 0){
                        response.sendRedirect("/");
                    }
                    request.setAttribute("checkoutProducts", checkoutProducts);
                    request.getRequestDispatcher("/WEB-INF/products/checkout.jsp").forward(request, response);
                    return;
                }
            }
            response.sendRedirect("/");
        } catch (SQLException ignored) {
            response.sendRedirect("/");
        }
    }
}
