package Controller.Products;

import Utilities.Config;
import Utilities.FileEditing;
import DAO.ProductDAO;
import Model.Product;
import Model.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.SQLException;

@WebServlet(name = "/Products/Delete", urlPatterns = { "/products/delete" })
public class Delete extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        if (request.getParameter("button").equals("Delete")){
            HttpSession session = request.getSession(false);
            Product product = (Product) session.getAttribute("deleteItem");
            FileEditing.deleteFile(request, product);
            session.setAttribute("Error", Config.PRODUCT_DELETED);
            try {
                ProductDAO.deleteProductById(product.getId());
            } catch (SQLException e) {
                if (Config.DEBUG){
                    e.printStackTrace();
                }
                session.setAttribute("Error", Config.SOMETHING_WRONG);
                response.sendRedirect("/products");
            }
        }
        response.sendRedirect("/products");
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession(false);
        request.setAttribute("title", Config.TITLE_DELETE_PRODUCT);
        if (session != null) {
            User user = (User) session.getAttribute("User");
            if (user != null) {
                if(user.getRole()>=1) {
                    try {
                        request.setAttribute("user", user);
                        request.setAttribute("logged", true);
                        if(request.getParameter("Id") == null){
                            request.getRequestDispatcher("/WEB-INF/products/delete.jsp").forward(request, response);
                            return;
                        }
                        int id = Integer.parseInt(request.getParameter("Id"));
                        Product product = ProductDAO.getProductById(id);
                        if (product != null) {
                            session.setAttribute("deleteItem", product);
                            response.sendRedirect("/products/delete");
                            return;
                        }
                    } catch (SQLException e) {
                        if (Config.DEBUG){
                            e.printStackTrace();
                        }
                    }
                }
            }
        }
        response.sendRedirect("/products");
    }
}
