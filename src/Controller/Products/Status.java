package Controller.Products;

import DAO.ProductDAO;
import Model.ProductCart;
import Model.Purchase;
import Model.User;
import Utilities.Config;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

@WebServlet(name = "/Products/Status", urlPatterns = {"/products/status"})
public class Status extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession(false);
        try{
            int purchaseId = Integer.parseInt(request.getParameter("purchaseId"));
            int status = Integer.parseInt(request.getParameter("status"));
            if(ProductDAO.updateStatus(purchaseId,
                    status)){
                session.setAttribute("Error", Config.PURCHASE_UPDATED);
                response.sendRedirect("/products/status");
            }
        }catch(SQLException e){
            e.printStackTrace();
            session.setAttribute("Error", Config.SOMETHING_WRONG);
            response.sendRedirect("/");
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession(false);
        request.setAttribute("title", Config.TITLE_PURCHASE_STATUS);
        try {
            if (session != null) {
                User user = (User) session.getAttribute("User");
                if (user != null) {
                    if (user.getRole() >= 2) {
                        ArrayList<Purchase> purchases = ProductDAO.getAllPurchases();
                        request.setAttribute("purchases", purchases);
                        request.setAttribute("user", user);
                        request.setAttribute("logged", true);
                        request.getRequestDispatcher("/WEB-INF/products/status.jsp").forward(request, response);
                    }
                }
            }
        }catch(SQLException e){
            e.printStackTrace();
            response.sendRedirect("/");
        }
        response.sendRedirect("/");
    }
}
