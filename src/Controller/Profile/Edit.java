package Controller.Profile;

import DAO.UserDAO;
import Model.User;
import Utilities.Config;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.SQLException;

@WebServlet(name = "Edit", urlPatterns = { "/profile/edit" })
public class Edit extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String firstName = request.getParameter("firstName");
        HttpSession session = request.getSession();
        if(firstName.length() < 1) {
            session.setAttribute("Error", Config.NO_FIRST_NAME_ENTERED);
            response.sendRedirect("/profile/edit");
            return;
        }
        if(firstName.length() > 30){
            session.setAttribute("Error", Config.FIRST_NAME_TOO_LONG);
            response.sendRedirect("/profile/edit");
            return;
        }
        String lastName = request.getParameter("lastName");
        if(lastName.length() < 1){
            session.setAttribute("Error", Config.NO_LAST_NAME_ENTERED);
            response.sendRedirect("/profile/edit");
            return;
        }
        if(lastName.length() > 30){
            session.setAttribute("Error", Config.LAST_NAME_TOO_LONG);
            response.sendRedirect("/profile/edit");
            return;
        }
        String email = request.getParameter("email");
        if(email.length() < 1){
            session.setAttribute("Error", Config.NO_EMAIL_ENTERED);
            response.sendRedirect("/profile/edit");
            return;
        }
        if(email.length() > 30){
            session.setAttribute("Error", Config.EMAIL_TOO_LONG);
            response.sendRedirect("/profile/edit");
            return;
        }
        String town = request.getParameter("town");
        if(town.length() < 1){
            session.setAttribute("Error", Config.NO_TOWN_ENTERED);
            response.sendRedirect("/profile/edit");
            return;
        }
        if(town.length() > 30){
            session.setAttribute("Error", Config.TOWN_TOO_LONG);
            response.sendRedirect("/profile/edit");
            return;
        }
        String address = request.getParameter("address");
        if(address.length() < 1){
            session.setAttribute("Error", Config.NO_ADDRESS_ENTERED);
            response.sendRedirect("/profile/edit");
            return;
        }
        if(address.length() > 30){
            session.setAttribute("Error", Config.ADDRESS_TOO_LONG);
            response.sendRedirect("/profile/edit");
            return;
        }
        String telNum = request.getParameter("telNum");
        if(telNum.length() < 1){
            session.setAttribute("Error", Config.NO_PHONE_ENTERED);
            response.sendRedirect("/profile/edit");
            return;
        }
        if(telNum.length() > 15){
            session.setAttribute("Error", Config.PHONE_TOO_LONG);
            response.sendRedirect("/profile/edit");
            return;
        }
        User user = (User)session.getAttribute("User");
        try {
            if (UserDAO.updateUserInfo(email, firstName, lastName, address, town, telNum, user.getId())) {
                try {
                    user = UserDAO.getUserById(user.getId());
                    session.setAttribute("Error", Config.PROFILE_UPDATED);
                    session.removeAttribute("User");
                    session.setAttribute("User", user);
                }catch (SQLException e){
                    session.setAttribute("Error", Config.SOMETHING_WRONG);
                }
            } else {
                session.setAttribute("Error", Config.SOMETHING_WRONG);
            }
            response.sendRedirect("/profile");
        }catch (SQLException e){
            e.printStackTrace();
        }
    }


    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession(false);
        request.setAttribute("title", Config.TITLE_EDIT_PROFILE);
        if(session != null){
            User user = (User)session.getAttribute("User");
            if (user != null) {
                request.setAttribute("user", user);
                request.setAttribute("logged", true);
                request.getRequestDispatcher("/WEB-INF/profileEdit.jsp").forward(request, response);
                return;
            }
        }
        response.sendRedirect("/");
    }


}
