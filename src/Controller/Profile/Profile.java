package Controller.Profile;

import DAO.ProductDAO;
import Model.ProductCart;
import Model.Purchase;
import Model.User;
import Utilities.Config;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

@WebServlet(name = "/Profile", urlPatterns = { "/profile" })
public class Profile extends HttpServlet {
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession(false);
        request.setAttribute("title", Config.TITLE_PROFILE);
        if(session != null) {
            User user = (User) session.getAttribute("User");
            if (user != null) {
                try {
                    ArrayList<Purchase> userPurchases = ProductDAO.getUserPurchases(user);
                    ArrayList<ProductCart> purchasedProducts = ProductDAO.getPurchasedCartProducts(user);
                    request.setAttribute("userPurchases", userPurchases);
                    request.setAttribute("purchasedProducts", purchasedProducts);
                    request.setAttribute("user", user);
                    request.setAttribute("logged", true);
                    request.getRequestDispatcher("/WEB-INF/profile.jsp").forward(request, response);
                    return;

                } catch (SQLException ignored) {
                    response.sendRedirect("/");
                    return;
                }

            }
        }
        response.sendRedirect("/");
    }
}
