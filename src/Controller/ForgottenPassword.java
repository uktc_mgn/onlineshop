package Controller;

import DAO.UserDAO;
import Utilities.Config;
import Utilities.FileEditing;
import Utilities.SendEmail;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.SQLException;

@WebServlet(name = "ForgottenPassword", urlPatterns = {"/forgottenPassword"})
public class ForgottenPassword extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        String userEmail = request.getParameter("userEmail");
        String verificationCode = FileEditing.genRandomString(6);

        try {
            if (UserDAO.checkForExistingEmail(userEmail)) {
                session.setAttribute("ForgottenPasswordEmail", userEmail);
                UserDAO.addVerificationCodeToDB(verificationCode, userEmail);

                SendEmail.sendEmail(userEmail, Config.EMAIL_SUBJECT, Config.EMAIL_MESSAGE + verificationCode);
                session.setAttribute("Error", Config.EMAIL_SENT);
                response.sendRedirect("/resetForgottenPassword");
            } else {
                session.setAttribute("Error", "Invalid email!");
                response.sendRedirect("/login");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setAttribute("title", Config.TITLE_FORGOTTEN_PASSWORD);
        request.setAttribute("logged", false);
        request.getRequestDispatcher("/WEB-INF/forgottenPassword.jsp").forward(request, response);
    }
}
