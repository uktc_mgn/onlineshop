package Controller.Users;

import DAO.UserDAO;
import Model.User;
import Utilities.Config;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.SQLException;

@WebServlet(name = "/Users/Demote", urlPatterns = {"/users/demote"})
public class Demote extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        HttpSession session = request.getSession();
        int userToDemoteId = Integer.parseInt(request.getParameter("demoteUserId"));
        int userToDemoteCurrentRole = Integer.parseInt(request.getParameter("demoteCurrentRole"));

        try {
            if (UserDAO.demoteUser(userToDemoteId, userToDemoteCurrentRole)) {
                session.setAttribute("Error", Config.USER_DEMOTED);
                response.sendRedirect("/users");
            } else {
                session.setAttribute("Error", Config.SOMETHING_WRONG);
                response.sendRedirect("/users");
            }
        } catch (SQLException e) {
            e.printStackTrace();
            session.setAttribute("Error", Config.SOMETHING_WRONG);
            response.sendRedirect("/users");
        }
    }
}

