package Controller.Users;

import DAO.UserDAO;
import Model.User;
import Utilities.Config;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.SQLException;

@WebServlet(name = "/Users/Promote", urlPatterns = { "/users/promote" })
public class Promote extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        HttpSession session=request.getSession();
        int userToPromoteId=Integer.parseInt(request.getParameter("promoteUserId"));
        int userToPromoteCurrentRole=Integer.parseInt(request.getParameter("promoteCurrentRole"));

        try {
            if(UserDAO.promoteUser(userToPromoteId,userToPromoteCurrentRole)){
                session.setAttribute("Error", Config.USER_PROMOTED);
                response.sendRedirect("/users");
            }else{
                session.setAttribute("Error", Config.SOMETHING_WRONG);
                response.sendRedirect("/users");
            }
        } catch (SQLException e) {
            e.printStackTrace();
            session.setAttribute("Error", Config.SOMETHING_WRONG);
            response.sendRedirect("/users");
        }

    }
}
