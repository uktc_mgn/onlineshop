package Controller.Users;

import DAO.UserDAO;
import Model.User;
import Utilities.Config;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

@WebServlet(name = "/Users/Index", urlPatterns = { "/users" })
public class Index extends HttpServlet {

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
       try {
           HttpSession session = request.getSession(false);
           request.setAttribute("title", Config.TITLE_EDIT_USERS);
           ArrayList<User> users = UserDAO.getAllUsers();
           request.setAttribute("users", users);
           if (session != null) {
               User user = (User) session.getAttribute("User");
               if (user != null) {
                   if(user.getRole()>=2) {
                       request.setAttribute("user", user);
                       request.setAttribute("logged", true);
                       request.getRequestDispatcher("/WEB-INF/editUsers.jsp").forward(request, response);
                       return;
                   }
               }
           }
           response.sendRedirect("/");
       }catch (SQLException e) {
           e.printStackTrace();
       }
    }

}
