package Controller;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import DAO.UserDAO;
import Model.User;
import Utilities.Config;

@WebServlet(name = "ChangePassword", urlPatterns = { "/changePassword" })
public class ChangePassword extends HttpServlet {
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession();
		String password = request.getParameter("password");
		if (password.length() < 1) {
			session.setAttribute("Error", Config.NO_PASSWORD_ENTERED);
			response.sendRedirect("/changePassword");
			return;
		}
		if (!password.matches("^(?=.*[\\S])(?=.*[0-9]).+")) {
			session.setAttribute("Error", Config.PASSWORD_UPPER_AND_DIGIT);
			response.sendRedirect("/changePassword");
			return;
		}
		if (password.length() < 6) {
			session.setAttribute("Error", Config.PASSWORD_UNDER_LENGTH);
			response.sendRedirect("/changePassword");
			return;
		}
		if (password.length() > 30) {
			session.setAttribute("Error", Config.PASSWORD_TOO_LONG);
			response.sendRedirect("/changePassword");
			return;
		}
		String confirmPassword =  request.getParameter("confirmPassword");
		if (confirmPassword.length() < 1) {
			session.setAttribute("Error", Config.PASSWORD_NOT_CONFIRMED);
			response.sendRedirect("/changePassword");
			return;
		}
		if(!password.equals(confirmPassword)){
			session.setAttribute("Error", Config.PASSWORD_CONFIRM_DIFFERENT);
			response.sendRedirect("/changePassword");
			return;
		}

		User user = (User) session.getAttribute("User");
		String oldPassword = request.getParameter("oldPassword");
		
		if (password.equals(confirmPassword)) {

			String changePasswordMsg = "";
			try {
				changePasswordMsg = UserDAO.changePass(user.getId(), confirmPassword, oldPassword);
				System.out.println(changePasswordMsg);
			} catch (SQLException e) {
				session.setAttribute("Error", Config.PASSWORD_NOT_CHANGED);
				response.sendRedirect("/changePassword");
				return;
			}

			session.removeAttribute("User");

		} else {
			session.setAttribute("Error", Config.WRONG_PASSWORD);
			response.sendRedirect("/changePassword");
			return;
		}
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.getWriter().append("Served at: ").append(request.getContextPath());
		HttpSession session = request.getSession(false);
		request.setAttribute("title", Config.TITLE_CHANGE_PASSWORD);
		if (session != null) {
			User user = (User) session.getAttribute("User");
			if (user != null) {
				request.setAttribute("user", user);
				request.setAttribute("logged", true);
				request.getRequestDispatcher("/WEB-INF/changePassword.jsp").forward(request, response);
				return;
			}
		}
		response.sendRedirect("/");

	}

}
