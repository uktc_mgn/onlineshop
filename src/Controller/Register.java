package Controller;

import DAO.UserDAO;
import Model.Password;
import Model.User;
import Utilities.Config;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.*;

@WebServlet(name = "Register", urlPatterns = { "/register" })
public class Register extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String firstName = (String) request.getParameter("firstName");
        HttpSession session = request.getSession();
        if(firstName.length() < 1) {
            session.setAttribute("Error", Config.NO_FIRST_NAME_ENTERED);
            response.sendRedirect("/register");
            return;
        }
        if(firstName.length() > 30){
            session.setAttribute("Error", Config.FIRST_NAME_TOO_LONG);
            response.sendRedirect("/register");
            return;
        }
        String lastName = request.getParameter("lastName");
        if(lastName.length() < 1){
            session.setAttribute("Error", Config.NO_LAST_NAME_ENTERED);
            response.sendRedirect("/register");
            return;
        }
        if(lastName.length() > 30){
            session.setAttribute("Error", Config.LAST_NAME_TOO_LONG);
            response.sendRedirect("/register");
            return;
        }
        String email = request.getParameter("email");
        if(email.length() < 1){
            session.setAttribute("Error", Config.NO_EMAIL_ENTERED);
            response.sendRedirect("/register");
            return;
        }
        if(email.length() > 30){
            session.setAttribute("Error", Config.EMAIL_TOO_LONG);
            response.sendRedirect("/register");
            return;
        }
        String password = (String) request.getParameter("password");
        if(password.length() < 1){
            session.setAttribute("Error", Config.NO_PASSWORD_ENTERED);
            response.sendRedirect("/register");
            return;
        }
        if(!password.matches("^(?=.*[\\S])(?=.*[0-9]).+")){
            session.setAttribute("Error", Config.PASSWORD_UPPER_AND_DIGIT);
            response.sendRedirect("/register");
            return;
        }
        if(password.length() < 6) {
            session.setAttribute("Error", Config.PASSWORD_UNDER_LENGTH);
            response.sendRedirect("/register");
            return;
        }
        if(password.length() > 30){
            session.setAttribute("Error", Config.PASSWORD_TOO_LONG);
            response.sendRedirect("/register");
            return;
        }
        String confirmPassword = (String) request.getParameter("confirmPassword");
        if(confirmPassword.length() < 1){
            session.setAttribute("Error", Config.PASSWORD_NOT_CONFIRMED);
            response.sendRedirect("/register");
            return;
        }
        if(!password.equals(confirmPassword)){
            session.setAttribute("Error", Config.PASSWORD_CONFIRM_DIFFERENT);
            response.sendRedirect("/register");
            return;
        }
        String town = request.getParameter("town");
        if(town.length() < 1){
            session.setAttribute("Error", Config.NO_TOWN_ENTERED);
            response.sendRedirect("/register");
            return;
        }
        if(town.length() > 30){
            session.setAttribute("Error", Config.TOWN_TOO_LONG);
            response.sendRedirect("/register");
            return;
        }
        String address = request.getParameter("address");
        if(address.length() < 1){
            session.setAttribute("Error", Config.NO_ADDRESS_ENTERED);
            response.sendRedirect("/register");
            return;
        }
        if(address.length() > 30){
            session.setAttribute("Error", Config.ADDRESS_TOO_LONG);
            response.sendRedirect("/register");
            return;
        }
        String telNum = request.getParameter("telNum");
        if(telNum.length() < 1){
            session.setAttribute("Error", Config.NO_PHONE_ENTERED);
            response.sendRedirect("/register");
            return;
        }
        if(telNum.length() > 15){
            session.setAttribute("Error", Config.PHONE_TOO_LONG);
            response.sendRedirect("/register");
            return;
        }
        try {
            if (UserDAO.createUser(email, password, firstName, lastName, address, town, telNum)) {
                session.setAttribute("email", email);
                session.setAttribute("Error", Config.ACCOUNT_CREATED);
                response.sendRedirect("/login");
                return;
            } else {
                session.setAttribute("Error", Config.SOMETHING_WRONG);
                response.sendRedirect("/");
                return;
            }
        }catch (SQLException ignored){}
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession(false);
        request.setAttribute("title", Config.TITLE_REGISTER);
        if (session != null) {
            User user = (User) session.getAttribute("User");
            if (user != null) {
                response.sendRedirect("/");
                return;
            }
        }
        request.setAttribute("logged", false);
        request.getRequestDispatcher("/WEB-INF/register.jsp").forward(request, response);
    }

    private void sendError(HttpServletRequest request, HttpServletResponse response, String error){
        request.setAttribute("error", error);
        RequestDispatcher rd=request.getRequestDispatcher("/WEB-INF/register.jsp");
        try {
            rd.forward(request, response);
        } catch (ServletException | IOException e) {
            e.printStackTrace();
        }
    }
}
