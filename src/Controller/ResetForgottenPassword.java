package Controller;

import DAO.UserDAO;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.SQLException;


@WebServlet(name = "ResetForgottenPassword", urlPatterns = { "/resetForgottenPassword" })
public class ResetForgottenPassword extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        String verificationCode=request.getParameter("verificationCode");
        String newPassword = request.getParameter("newPassword");
        String confirmNewPassword=request.getParameter("confirmNewPassword");



        String userEmail=(String)session.getAttribute("ForgottenPasswordEmail");
        try {
            int user_id=UserDAO.getUserIdByEmail(userEmail);
            if(newPassword.equals(confirmNewPassword)) {
                if (UserDAO.checkIfTheVerificationCodeIsValid(user_id, verificationCode, newPassword)) {
                    session.setAttribute("Error","Password changed!");
                    response.sendRedirect("/login");

                }else {
                    session.setAttribute("Error","The code expired");
                    response.sendRedirect("/login");
                }
            }else {
                session.setAttribute("Error","Passwords do not match");
                response.sendRedirect("/resetForgottenPassword");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }


    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setAttribute("title", "Reset Forgotten Password");
        request.getRequestDispatcher("/WEB-INF/resetForgottenPassword.jsp").forward(request, response);
        return;
    }
}
