package Controller;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import DAO.ProductDAO;
import Model.ProductCart;
import Model.User;
import Utilities.Config;

@WebServlet(name = "/Cart", urlPatterns = {"/cart"})
public class Cart extends HttpServlet {

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            HttpSession session = request.getSession(false);
            request.setAttribute("title", Config.TITLE_CART);

            if (session != null) {
                User user = (User) session.getAttribute("User");
                if (user != null) {
                    ArrayList<ProductCart> productsCart = ProductDAO.getCartProducts(user);
                    request.setAttribute("productsCart", productsCart);
                    request.setAttribute("user", user);
                    request.setAttribute("logged", true);
                    request.getRequestDispatcher("/WEB-INF/cart.jsp").forward(request, response);
                    return;
                }
            }
            response.sendRedirect("/");

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws IOException {
        int quantityToRemove = Integer.parseInt(request.getParameter("quantityToRemove"));
        int productId = Integer.parseInt(request.getParameter("productId"));
        HttpSession session = request.getSession(false);
        User user = (User) session.getAttribute("User");
        ProductDAO.removeFromCart(user, productId, quantityToRemove);
        response.setContentType("text/plain");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().write(Config.SUCCESSFULLY_REMOVED_FROM_CART);

    }

}
