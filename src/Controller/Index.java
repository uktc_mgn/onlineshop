package Controller;

import DAO.ProductDAO;
import Model.Product;
import Model.User;
import Utilities.Config;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

@WebServlet(name = "Index", urlPatterns = {""})
public class Index extends HttpServlet {

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        try {
            HttpSession session = request.getSession(false);
            request.setAttribute("title", Config.TITLE_HOME);
            request.setAttribute("products", ProductDAO.getProducts());
            if (session != null) {
                User user = (User) session.getAttribute("User");
                if (user != null) {
                    request.setAttribute("user", user);
                    request.setAttribute("logged", true);
                    request.getRequestDispatcher("/WEB-INF/index.jsp").forward(request, response);
                    return;
                }
            }
            request.setAttribute("logged", false);
            request.getRequestDispatcher("/WEB-INF/index.jsp").forward(request, response);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
