package Utilities;

class MySQLConfig {
    static final String MYSQL_USER = "root";
    static final String MYSQL_URL = "jdbc:mysql://localhost/shop";
    static final String MYSQL_PASSWORD = "";
    static final String MYSQL_DRIVER = "com.mysql.jdbc.Driver";
}
