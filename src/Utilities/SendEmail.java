package Utilities;

import java.util.*;
import javax.mail.*;
import javax.mail.internet.*;
import javax.mail.internet.MimeMessage;

public class SendEmail {

    public static void sendEmail(String toEmail, String subjectEmail, String message) {
        try {
            Properties props = System.getProperties();

            props.put("mail.smtp.starttls.enable", "true");
            props.put("mail.smtp.host", Config.EMAIL_HOST);
            props.put("mail.smtp.port", "587");
            props.put("mail.smtp.auth", "true");
            props.put("mail.smtp.starttls.required", "true");

            java.security.Security.addProvider(new com.sun.net.ssl.internal.ssl.Provider());
            Session mailSession = Session.getDefaultInstance(props, null);
            Message msg = new MimeMessage(mailSession);
            msg.setFrom(new InternetAddress(Config.EMAIL_ADDRESS));
            InternetAddress[] address = {new InternetAddress(toEmail)};
            msg.setRecipients(Message.RecipientType.TO, address);
            msg.setSubject(subjectEmail);
            msg.setSentDate(new Date());
            msg.setText(message);

            Transport transport = mailSession.getTransport("smtp");
            transport.connect(Config.EMAIL_HOST, Config.EMAIL_USER, Config.EMAIL_PASS);
            transport.sendMessage(msg, msg.getAllRecipients());
            transport.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }


}
