package Utilities;

public class Config {
    public static final boolean DEBUG = false;

    public static final String SITENAME = "Your Site Name";
    public static final String SUCCESSFULLY_REMOVED_FROM_CART = "The product was successfully removed from your cart!";
    public static final String NEW_SAME_AS_OLD = "Your new password cannot be the same as your old password!";
    public static final String SOMETHING_WRONG = "Something went wrong!";
    public static final String PROFILE_UPDATED = "Your profile has been successfully updated!";
    public static final String ACCOUNT_CREATED = "Your account has been successfully created!";
    public static final String USER_DELETED = "The user has been deleted!";
    public static final String USER_DEMOTED = "The user has been demoted!";
    public static final String USER_PROMOTED = "The user has been promoted!";
    public static final String PURCHASE_FINISHED = "Your purchase has been completed! You can check the status in your profile page.";

    public static final String PASSWORD_CHANGED = "Password changed successfully!";
    public static final String WRONG_PASSWORD = "Wrong password!";
    public static final String INCORRECT_PASSWORD = "Incorrect password!";
    public static final String INCORRECT_OLD_PASSWORD = "Your old password is incorrect!";
    public static final String PASSWORD_NOT_CONFIRMED = "You forgot to confirm your password!";
    public static final String PASSWORD_NOT_CHANGED = "Your password couldn't be changed!";
    public static final String PASSWORD_UNDER_LENGTH = "Your password needs to be at least 6 symbols!";
    public static final String PASSWORD_UPPER_AND_DIGIT = "Your password needs to contain atleast 1 digit and 1 letter!";
    public static final String PASSWORD_CONFIRM_DIFFERENT = "The confirmation password is not the same as the entered password!";
    public static final String TOO_MUCH_QUANTITY = "The quantity you tried to add exceeds the product's available quantity!";
    public static final String PRODUCT_ADDED = "The product has been successfully added to your cart!";
    public static final String PRODUCT_EDITED = "The product has been successfully edited!";
    public static final String PRODUCT_DELETED = "The product has been successfully deleted!";
    public static final String PRODUCT_CREATED = "The product has been successfully created!";
    public static final String PURCHASE_UPDATED = "The purchase status has been successfully updated!";


    public static final String EMAIL_SUBJECT = "Forgotten password";
    public static final String EMAIL_MESSAGE = "The code to reset your password is: ";
    public static final String EMAIL_SENT = "An email has been sent to your email address!";

    public static final String NO_PASSWORD_ENTERED = "You forgot to enter your password!";
    public static final String NO_FIRST_NAME_ENTERED = "You forgot to enter your first name!";
    public static final String NO_LAST_NAME_ENTERED = "You forgot to enter your last name!";
    public static final String NO_EMAIL_ENTERED = "You forgot to enter your last name!";
    public static final String NO_TOWN_ENTERED = "You forgot to enter your town!";
    public static final String NO_ADDRESS_ENTERED = "You forgot to enter your address!";
    public static final String NO_PHONE_ENTERED = "You forgot to enter your phone number!";

    public static final String PASSWORD_TOO_LONG = "Your password needs to be less than 30 symbols!";
    public static final String FIRST_NAME_TOO_LONG = "Your first name is too long!";
    public static final String LAST_NAME_TOO_LONG = "Your last name is too long!";
    public static final String EMAIL_TOO_LONG = "Your last name is too long!";
    public static final String TOWN_TOO_LONG = "Your town is too long!";
    public static final String ADDRESS_TOO_LONG = "Your address is too long!";
    public static final String PHONE_TOO_LONG = "Your phone number is too long!";

    //TITLES
    public static final String TITLE_HOME = "Home";
    public static final String TITLE_PROFILE = "Profile";
    public static final String TITLE_PRODUCTS = "Products";
    public static final String TITLE_EDIT_USERS = "Manage Users";
    public static final String TITLE_EDIT_PRODUCT = "Edit Product";
    public static final String TITLE_CREATE_PRODUCT = "Create Product";
    public static final String TITLE_DELETE_PRODUCT = "Delete Product";
    public static final String TITLE_EDIT_PROFILE = "Profile";
    public static final String TITLE_LOGIN = "Login";
    public static final String TITLE_REGISTER = "Register";
    public static final String TITLE_CHANGE_PASSWORD = "Change Password";
    public static final String TITLE_CART = "Cart";
    public static final String TITLE_CHECKOUT = "Checkout";
    public static final String TITLE_FORGOTTEN_PASSWORD = "Forgotten Password";
    public static final String TITLE_PURCHASE_STATUS = "Purchase Status";

























    static final String EMAIL_HOST ="smtp.gmail.com" ;
    static final String EMAIL_USER = "OnlineShopUKTC";
    static final String EMAIL_PASS = "!onlineShop@";
    static final String EMAIL_ADDRESS = "onlineshopuktc@gmail.com";

}
