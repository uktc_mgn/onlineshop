package Utilities;

import java.sql.*;

public class MySQL {
    private static Connection connection;
    public static void connect(){
        try {
            if (connection != null && !connection.isClosed()) {
                return;
            }
            Class.forName(MySQLConfig.MYSQL_DRIVER);
            connection = DriverManager.getConnection(MySQLConfig.MYSQL_URL, MySQLConfig.MYSQL_USER, MySQLConfig.MYSQL_PASSWORD);
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
    public static void closeConnection() throws SQLException {
        connection.close();
    }

    public static Connection getConnection() {
        return connection;
    }
}
