package Utilities;

import Model.Product;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.Part;
import java.io.File;
import java.io.IOException;
import java.util.Random;

public class FileEditing {
    public static String genRandomString(int length) {
        Random rnd = new Random();
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < length; i++) {
            switch (rnd.nextInt(3)) {
                case 0: {
                    sb.append(rnd.nextInt(10));
                    break;
                }
                case 1: {
                    char a = (char) (rnd.nextInt(25) + 65);
                    sb.append(a);
                    break;
                }
                case 2: {
                    char a = (char) (rnd.nextInt(25) + 97);
                    sb.append(a);
                    break;
                }
            }
        }
        return sb.toString();
    }

    public static String getFileName(Part part) {
        String contentDisp = part.getHeader("content-disposition");
        String[] tokens = contentDisp.split(";");
        for (String token : tokens) {
            if (token.trim().startsWith("filename")) {
                return token.substring(token.indexOf("=") + 2, token.length() - 1);
            }
        }
        return "";
    }

    public static String createFile(HttpServletRequest request) throws IOException, ServletException {
        String fileName = FileEditing.getFileName(request.getPart("pic"));
        StringBuilder sb = new StringBuilder();
        sb.append(fileName);
        sb.indexOf(".");
        sb.replace(0, sb.indexOf("."), FileEditing.genRandomString(12));
        fileName = sb.toString();
        String uploadFilePath = request.getServletContext().getRealPath("") + "\\resources\\images\\";
        File fileSaveDir = new File(uploadFilePath);
        if (!fileSaveDir.exists()) {
            fileSaveDir.mkdirs();
        }
        Part part = request.getPart("pic");
        part.write(uploadFilePath + File.separator + fileName);
        return fileName;
    }

    public static void deleteFile(HttpServletRequest request, Product product) throws IOException, ServletException {
        String uploadFilePath = request.getServletContext().getRealPath("") + "\\resources\\images\\";
        File file = new File(uploadFilePath + product.getImageName());
        file.delete();
    }
}
